<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStatusToAppoinment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('appoinment', function (Blueprint $table) {
            $table->string('user_id');
            $table->string('field');
            $table->string('image');
            $table->string('order_id');
            $table->string('status');
            $table->string('reason_newappoin');
            $table->string('otp');
            $table->string('track_point');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('appoinment', function (Blueprint $table) {
            //
        });
    }
}
