<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBrandModel extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('brand_model', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('brand_id');
            $table->string('brand_model_name');
            $table->string('brand_image');
            $table->string('actualprice');
            $table->string('sellingprice');
            $table->string('screenproPrice');
            $table->string('about',5000)->nullable();
           
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('brand_model');
    }
}
