<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Offer extends Model
{
    protected $fillable = ['offer_name', 'offer_code', 'offer_amount', 'offer_type', 'offer_on', 'offer_status'];
}
