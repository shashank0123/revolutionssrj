<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class brandModel extends Model
{
    protected $table='brand_model';

    protected $fillable=['brand_id','brand_model_name','brand_image','actualprice','sellingprice','screenproPrice','model_title','about'];

    
}
