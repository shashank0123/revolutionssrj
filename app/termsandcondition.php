<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class termsandcondition extends Model
{
    protected $table='termsandcondition';

    protected $fillable=['about'];
}
