<?php

namespace App\Http\Controllers;

use App\Testimonial;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Log;

class TestimonialController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request)
  {
    $order_by_column = 'created_at';
    $order_by_value = 'desc';
    if($request->has('service_name') || $request->has('service_status') || $request->has('order_by_column') || $request->has('order_by_value'))
      {
        $query = (new Testimonial)->newQuery();
          if($request->has('service_name') && $request->input('service_name') != ''){
            $query->where('service_name', 'Like', '%' . $request->input('service_name') . '%');
          }

          if($request->has('service_status') && $request->input('service_status') != 'all' && $request->input('service_status') != ''){
            $query->where('status', $request->input('service_status') );
          }

          if($request->has('order_by_column') && $request->input('order_by_column') != 'all' && $request->input('order_by_column') != ''){
            $order_by_column = $request->input('order_by_column');
          }

          if($request->has('order_by_value') && $request->input('order_by_value') != 'all' && $request->input('order_by_value') != ''){
            $order_by_value = $request->input('order_by_value');
          }

          $testimonials = $query->orderBy($order_by_column, $order_by_value)->paginate(10);
      } else {
          $testimonials = Testimonial::orderBy('created_at', 'desc')->paginate(10);
      }
      return view('AdminPanel.testimonial.index', compact('testimonials'));
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
      return view('AdminPanel.testimonial.create');
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    // Log::info($request);
    $validator = Validator::make($request->toArray(), [
      'client_name'=>'required',
      'image_url'=>'required|mimes:jpeg,png,jpg,gif,svg|max:2000',
      'rating'=>'required',
      'review'=>'required',
      'status'=>'required',
    ]);

    if((count($validator->messages()) > 0) || ($validator->fails()))
       {
         return redirect()->back()->withErrors($validator->errors())->withInput($request->toArray());
       }
       if ($request->file('image_url')) {
           // Get image file
           $image = $request->file('image_url');
           $service_image =$this->imageUpload($image);
       }

       Testimonial::create([
         'client_name' => $request->client_name,
         'review' => $request->review,
         'rating' => $request->rating,
         'status' => $request->status,
         'city' => '',
         'image_url' => $service_image ]);

       return redirect('testimonial')->with('success', 'Sucessfully update the data');

  }

    /**
     * Display the specified resource.
     *
     * @param  \App\Testimonial  $testimonial
     * @return \Illuminate\Http\Response
     */
    public function show(Testimonial $testimonial)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Testimonial  $testimonial
     * @return \Illuminate\Http\Response
     */
    public function edit(Testimonial $testimonial)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Testimonial  $testimonial
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Testimonial $testimonial)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Testimonial  $testimonial
     * @return \Illuminate\Http\Response
     */
    public function destroy(Testimonial $testimonial)
    {
        //
    }
    /**
     * save image
     *
     * @param image file
     * @return uploaded image name
     */
     public function imageUpload($image)
     {
       // Make a image name based on user name and current timestamp
       $name = time();
       // Define folder path
       $folder = '/images/';
       // Make a file path where image will be stored [ folder path + file name + file extension]
       $filePath = $name. '.' . $image->getClientOriginalExtension();
       Log::info($filePath);
       // Upload image
       $file = $image->storeAs($folder, $name.'.'.$image->getClientOriginalExtension(), 'public');
       // $this->uploadOne($image, $folder, 'public', $name);
       // Set user profile image path in database to filePath
       return $filePath;
     }
}
