<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use App\slider;
use App\Testimonial;
use DB;
use Session;
use Redirect;
use App\brandDetails;
use App\brandModel;
use App\brandcolor;
use App\contact;
use App\AboutUs;
use App\AvailableStatus;
use App\termsandcondition;
use App\benifitabout;
use App\address;
use Response;
use App\questionanswer;
use App\appoinment;
use Illuminate\Support\Facades\Auth;
use App\order_track;
use Log;
use App\Service;
use App\ModelServices;
use App\Offer;
use App\Jobs\OrderInvoice;
use App\OrderInvoiceDTO;
use Mail;

class AdminApiController extends Controller

{

    public function cityList(){
        
        $city=AvailableStatus::where('status', 'Available')->orderBy('state')->get();
        if($city != null){
            foreach ($city as $key => $value) {
          $value['ImageUrl'] =  env('APP_URL').'/admin/images/'. $value->image;
        }
        }
        
        return Response::json(['status' =>'success','city'=>$city->toArray() ]);

    }



    public function searchCity(Request $request){

             $city = $request->searchQ;
        $city1 = AvailableStatus::where ( 'state', 'LIKE', '%' . $city . '%' )->orWhere ( 'pin', 'LIKE', '%' . $city . '%' )->orderBy('state')->get ();
    if (count ( $city1 ) > 0){

        return Response::json(['status' =>'success','city'=>$city1->toArray() ]);
        }else{
        return Response::json(['Woops!' =>'No data found']);

        }

    }

    public function AvailableStatus(Request $request){
        $selectCity=$request->cityName;
        session(['selectCity'=>$selectCity]);
        $avail=AvailableStatus::all()->where('state','=',$selectCity)->where('status','=','Available');
        return Response::json(array(
            'status' => 'success',
            'About' => $avail->toArray()),
            200
        );
    }

    public function previouscity(Request $request){
        $preCity=session::get('selectCity');
        $avail=AvailableStatus::all()->where('state','=',$preCity)->where('status','=','Available');
        return Response::json(array(
            'status' => 'success',
            'About' => $avail->toArray()),
            200
        );
    }


	public function AboutUs(){
    	$about=AboutUs::all();
    	$benifitabout=benifitabout::all();

    	return Response::json(array(
            'status' => 'success',
            'About' => $about->toArray(),
            'benifitabout'=>$benifitabout->toArray(),),
            200
        );
    }

    public function brands(){
        $brandName=brandDetails::orderBy('brand_name')->get();
        return Response::json(array(
            'status' => 'success',
            'Brand_Name' => $brandName->toArray()),200);

    }

      public function sliderImages(){
        $sliderImages = slider::all();
        foreach ($sliderImages as $key => $value) {
          $value['imageUrl'] =  env('APP_URL').'/admin/images/'. $value->image;
        }
        return Response::json(array(
            'status' => 'success',
            'sliderImages' => $sliderImages->toArray()),200);
    }
    
    // get Testimonial
    public function getTestimonial(){
      $testimonials = Testimonial::where('status', 'active')->get();
      foreach ($testimonials as $key => $value) {
        $value['imageUrl'] =  env('APP_URL').'/admin/'. $value->image_url;
      }
      return Response::json(array(
          'status' => 'success',
          'testimonials' => $testimonials->toArray()),200);
  }

    public function barandsmodel(Request $request){
        $brand_id=$request->id;
        $brandmodel=brandModel::all()->where('id','=',$brand_id);
        return Response::json(array(
            'Brand_Model' => $brandmodel->toArray()),200);



    }

    public function brandName(Request $request){
        $brand_id=$request->id;
        $model=brandDetails::all()->where('id','=',$brand_id);
        foreach($model as $key => $value){
            $brand=$value->brand_name;
            return Response::json(['success'=>$brand]);
        }


    }

    public function BrandDetails(Request $request){
    		$brandName=$request->BrandName;
            $brandName=brandDetails::where('brand_name','=',$brandName)->orderBy('brand_name')->get();

            if($brandName){
                foreach ($brandName as $key => $value) {
                $brand_id=$value->id;
            }
            $brandmodel=brandmodel::where('brand_id','=',$brand_id)
                                    // ->select('brand_model_name')
                                    // ->groupBy('brand_model_name')
                                    ->orderBy('brand_model_name')
                                    ->get();
            foreach ($brandmodel as $key => $value) {
                $brandmodel_id=$value->id;
            }
            if ($brandmodel) {
              $brandColor=brandcolor::all()->where('brand_model_id','=',$brandmodel_id);
            }else {
              $brandColor = new brandcolor();
            }


            return Response::json(array(
            'Brand_Name' => $brandName->toArray(),
            'brandmodel'=>$brandmodel->toArray(),
            'brandcolor'=>$brandColor->toArray()),200);
            } else {
                return Response::json(['error'=>'something is wrong with this']);
            }

    }

    public function brandColor(Request $request){

        $brand_model_id=$request->id;
        $color=brandcolor::all()->where('brand_model_id','=',$brand_model_id);
         return Response::json(array(
            'brandcolor'=>$color->toArray()),200);


    }
    // get model color on model selected
    public function getModelColor(Request $request)
    {
      $modelName = $request->model_name;
      $brandmodel=brandmodel::where('brand_model_name','=',$modelName)->first();
      if($brandmodel){
        $color=brandcolor::where('brand_model_id','=',$brandmodel->id)->get();
      }else {
        $color = new brandColor();
      }
       return Response::json(array(
          'brandcolor'=>$color->toArray()),200);
    }

    public function productList(Request $request){

        $productName=$request->productName;
        $brandName=brandDetails::all()->where('brand_name','=',$productName);
            foreach ($brandName as $key => $value) {
                $brand_id=$value->id;
            }
            $brandmodel=brandmodel::all()->where('brand_id','=',$brand_id);
            foreach ($brandmodel as $key => $value) {
                $brandmodel_id=$value->id;
            }

            $brandColor=brandcolor::all()->where('brand_model_id','=',$brandmodel_id);

            return Response::json(array(
            'Brand_Name' => $brandName->toArray(),
            'brandmodel'=>$brandmodel->toArray(),
            'brandcolor'=>$brandColor->toArray()),200);



    }


    public function fromRequest(Request $request,$token){

    	$brandName=$request->brandName;
    	$brandModel=$request->brandModel;

    	$brandModel=brandModel::all()->where('brand_model_name','=',$brandModel);
    	foreach ($brandModel as $key => $value) {
    		$price=$value->screenproPrice;


    	}


    	$brandcolor=$request->brandcolor;
    	session(['brandName'=>$brandName]);
    	session(['brandModel'=>$brandModel]);
    	session(['brandColor'=>$brandcolor]);
    	session(['price'=>$price]);


    	$data = array('name' =>$brandName ,'model'=>$brandModel,'color'=>$brandcolor );

    	return Response::json(['success' =>$data]);



    }

    public function contactUs(Request $request){
        $validator = Validator::make($request->all(), [
            'phone'=>'required|min:10|unique:users',
            'email'=>'required|email',
        ]);
if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);
        }
      $name=$request->name;
      $email=$request->email;
      $phone=$request->phone;
      $query=$request->query;
      $message=$request->message;

      $data=array('name' =>$name ,'email'=>$email,'phone'=>$phone,'query'=>$query,'message'=>$message,'created_at'=>NOW(),'updated_at'=>NOW());
      $data=DB::table('contact_us')->insert($data);
      if($data==true){
        return Response::json(['success' =>$data]);

    }else{
        return Response::json(['error'=>'something is wrong with this']);
    }


    }


    public function termsandcondition(){
    	$terms=termsandcondition::all();

    	return Response::json(array(
            'status' => 'success',
            'terms' => $terms->toArray()),
            200
        );
    }


//service details
    public function servicedetails($id){
    	$terms=termsandcondition::all();
    	$brandModel=brandModel::all()->where('brand_id','=',$id);
    	foreach ($brandModel as $key => $value) {
    		$color=$value->id;
    	}
    	$brandcolor=brandcolor::all()->where('brand_model_id','=',$color);
    	return Response::json(array(
            'status' => 'success',
            'brandModel' => $brandModel->toArray(),
        	'brandcolor'=>$brandcolor->toArray(),
        	'terms'=>$terms->toArray(),),
            200
        );

    }

    public function BrandAddress($id){
    	$brandModel=brandModel::all()->where('brand_id','=',$id);
    	$user_id=Auth::user()->id;
    	$address=address::all()->where('id','=',$user_id);
    	return Response::json(array(
            'status' => 'success',
            'brandModel' => $brandModel->toArray(),
        	'address'=>$address->toArray(),),
            200
        );


    }

    public function brandmodel(){

    $all=brandModel::all();
    foreach ($all as $key => $value) {
      $brandDetails = \DB::table('brand_details')->where('id', $value->brand_id)->first();
      if ($brandDetails) {
        $value['brand_name'] = $brandDetails->brand_name;
      }
    }
    return Response::json(['all'=>$all]);
}
//id will be brand id
//      public function appoinment(Request $request){
//          $validator = Validator::make($request->all(), [
//             'brandName'=>'required',
//             'brandModel'=>'required',

//         ]);
// if ($validator->fails()) {
//             return response()->json(['error'=>$validator->errors()], 401);
//         }
//         $field=$request->field;
//     	$user_id=Auth::user()->id;
//     	$username=Auth::user()->name;
//     	$email=Auth::user()->email;
//     	$phone=Auth::user()->mobile;
//     	$order_id=rand(100000,999999);

//     	$address=address::all()->where('user_id','=',$user_id)->where('saveAs','=',$field);
//     	foreach ($address as $key => $value) {
//     		$pin=$value->pincode;
//     		$completeAddress=$value->completeAddress;
//     		$saveAs=$value->saveAs;
//     	}
//     	$date=$request->date;
//     	$time=$request->time;

//         $brandname=$request->brandName;
//         $brandmodel=$request->brandModel;
//         $brandColor=$request->brandColor;
//         $price=$request->price;
//         $model=brandModel::all()->where('brand_model_name','=',$brandmodel);
//         foreach($model as $key => $value){
//             $image=$value->brand_image;

//         }


//     	$time=$service_time = $this->serviceStringToDate($request->date, $request->time);
//     	$data = array('user_id'=>$user_id,'username' =>$username ,'email'=>$email,'phone'=>$phone,'Brand_Name'=>$brandname,'Brand_Model'=>$brandmodel,'image'=>$image,'Brand_color'=>$brandColor,'price'=>$price,
//     		'address'=>$completeAddress,'pincode'=>$pin,'service_date'=>$date,'service_time'=>$time,'field'=>$saveAs,'order_id'=>$order_id,'created_at'=>$time, 'updated_at'=>$time );

//     	$insert=DB::table('appoinment')->insert($data);
//     	if($insert==true){
//     	    $order=new order_track;
//     	    $order->order_id=$order_id;
//     	    $order->user_id=$user_id;
//     	    $order->placed_order=1;
//     	    $order->save();
//     	    return Response::json(['success'=>$data]);
//     	}else{
//     		return response::json(['error'=>'something is wrong']);
//     	}

//     }

public function appoinment(Request $request){
    // Log::info($request);
         $validator = Validator::make($request->all(), [
            'brandName'=>'required',
            'brandModel'=>'required',

        ]);
if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);
        }
        $productData = collect();
        $field=$request->field;
    	$user_id=Auth::user()->id;
    	$username=Auth::user()->name;
    	$email=Auth::user()->email;
    	$phone=Auth::user()->mobile;

    	$address=address::all()->where('user_id','=',$user_id)->where('saveAs','=',$field);
    	foreach ($address as $key => $value) {
    		$pin=$value->pincode;
    		$completeAddress=$value->completeAddress;
    		$saveAs=$value->saveAs;
    	}
    	$date=$request->date;
    	$time=$request->time;

        $brandname=$request->brandName;
        $brandmodel=$request->brandModel;
        $brandColor=$request->brandColor;
        $price=$request->price;

        $time=$service_time = $this->serviceStringToDate($request->date, $request->time);

        // save multiple orders
        foreach (json_decode($request->cart) as $key => $cartData) {
            // Log::info($cartData->screenproPrice);
         $modelServices = ModelServices::where('model_id', $cartData->model_id)->first();
        //  Log::info($services);
          $service_id = $modelServices->id;
          $model=brandModel::where(['brand_model_name' => $cartData->modelName, 'id' => $cartData->model_id])->first();
        //   Log::info($model);

          $order_id=rand(100000,999999);

      	$data = array('user_id'=>$user_id,'username' =>$username ,'email'=>$email,'phone'=>$phone,'Brand_Name'=>$brandname,'Brand_Model'=>$brandmodel,
          'image'=>$model->brand_image, 'Brand_color'=>$brandColor, 'price'=> $cartData->screenproPrice, 'address'=>$completeAddress,
          'pincode'=>$pin,'service_date'=>$date,'service_time'=>$time,'field'=>$saveAs,'order_id'=>$order_id,
          'service_name' => $cartData->serviceName, 'offer_id' => $request->offer_id);

      	$insert=DB::table('appoinment')->insert($data);
            // $insert = 
    //   	
    // $productData->push($data);
    // array_push($productData, $data);
    $productData[] = $data;
        if ($insert == true) {
          $order=new order_track;
          $order->order_id=$order_id;
          $order->user_id=$user_id;
          $order->placed_order=1;
          $order->save();
        }else{
    		return response::json(['error'=>'something is wrong']);
    	    }
        }
        // send invoice email
        $invoiceData = new OrderInvoiceDTO();

        $invoiceData->logo =env('APP_URL').'/assets/mobile/image/spazeme-logo.png';
        $invoiceData->name = $username;
        $invoiceData->email = $email;
        $invoiceData->brand = $request->brandName;
        $invoiceData->model = $request->brandModel;
        $invoiceData->color = $request->brandColor;
        $invoiceData->total_price = $request->price;
        $invoiceData->address = $completeAddress;

        // $this->emailInvoice($invoiceData, $productData);
        // send email
        $arrayInvoice = array('name' => $username, 'email' =>$email, 'brand' =>$request->brandName, 'model' =>$request->brandModel, 'color' => $request->brandColor, 'total_price' => $request->price, 'address' => $completeAddress); 

        $this->emailInvoice($arrayInvoice, $productData);
        // send sms to user 
        $message = 'Your%20order%20has%20been%20booked%20successfully';
        $this->sendSMS($phone, $message);
        
        // send sms to admin 
        $message = $username.'('.$phone.') has requested for repairing '.$request->brandName.' '.$request->brandModel.' '.$request->brandColor;
        $message = str_replace(' ', '%20', $message );
        $this->sendSMS(8470095700, $message);

    // 	if($insert==true){

    	    return Response::json(['success'=>'item added succesfully']);
    // 	}else{
    // 		return response::json(['error'=>'something is wrong']);
    // 	}

    }

    public function orderDetails(){
        $user_id=Auth::user()->id;
        $order=appoinment::where('user_id','=',$user_id)->orderBy('created_at', 'desc')->get();
        return Response::json(['success'=>$order]);

    }

    public function deleteAppoinment(Request $request){
       $mobile=Auth::user()->mobile;
       $order_id=$request->order_id;

       $delete=appoinment::where('phone','=',$mobile)->where('order_id','=',$order_id)->delete();
       if($delete==true){
           return Response::json(['success'=>$delete]);
       }else{
            return Response::json(['message'=>'check order_id'],404);
       }
    }

    public function verifymobile(Request $request){
        $phone= $request->mobile; //Auth::user()->mobile;
        $order_id=$request->order_id;
        $mobile=$request->mobile;
        $otp = rand(100000, 999999);


                $otpen = str_replace(' ', '%20', $otp);
                $message = "This%20is%20your%20OTP%20$otpen%20to%20reset%20password%20of%20your%20indiancf%20account%20.Valid%20for%202%20hours%20";
                  $url = "http://103.247.98.91/API/SendMsg.aspx?uname=20190770&pass=d797g9t4&send=flgrth&dest=$mobile&msg=$message";
                  $url = "http://smpp.webtechsolution.co/http-tokenkeyapi.php?authentic-key=35376261636b73746167653030313536341569845354&senderid=SPAZME&route=4&number=$mobile&message=$message";
                   $c = curl_init();
                   curl_setopt($c,CURLOPT_RETURNTRANSFER,1);
                   curl_setopt($c,CURLOPT_HTTPGET ,1);

                   curl_setopt($c, CURLOPT_URL, $url);
                   $contents = curl_exec($c);
                 if (curl_errno($c)) {
                   echo 'Curl error: ' . curl_error($c);
                 }else{
                   curl_close($c);
                 }
        $user=DB::update('update appoinment  set otp=? where phone=? AND order_id=?',[$otp, $phone,$order_id]);
        if($user==true){

        return response()->json(['success'=>$mobile,'otp'=>$otp,'message'=>'check mobile phone for otp']);
      }else{
        return response()->json(['fail'=>$user]);

    }



    }

    public function UpdateAppoinmentNo(Request $request){
        $mobile=Auth::user()->mobile;
        $order_id=$request->order_id;
        $phone=$request->phone;
        $otp_get=$request->otp_get;

        $data=appoinment::all()->where('phone','=',$mobile)->where('order_id','=',$order_id);
        foreach($data as $key => $value){

            $otp=$value->otp;
        }
        if($otp==$otp_get){

        $update=DB::update('update appoinment set phone=?  where order_id=? AND phone=? ',[$phone,$order_id,$mobile]);
        if($update==true){
          return Response::json(['success'=>$update,'message'=>'update successfully']);
      }else{
            return Response::json(['message'=>'check order_id'],404);
      }
        }else
      return Response::json(['message'=>'wrong otp']);

    //return Response::json(['message'=>$mobile, $order_id,$phone,$otp_get]);
    }

// // reschedule order date and time
//     public function rescheduleOrder(Request $request){
//       $userId = Auth::user()->id;
//       $update = appoinment::where(['order_id'=> $request->order_id, 'user_id' =>$userId])
//                 ->update(['service_date'=> $request->date, 'service_time' => $request->time]);

//         if($update==true){
//           return Response::json(['success'=>$update,'message'=>'update successfully']);
//       }else{
//             return Response::json(['message'=>'check order_id'],404);
//       }
//     }


    public function F_Q(){

    	$data=questionanswer::all();
    	return Response::json(array(
    		'status'=>'success',
    		'F&Q'=>$data->toArray()
    	),200);
    }

    public function OrderHistory(){

        $phone=Auth::user()->mobile;
        $history=appoinment::where('phone','=',$phone)->where('status','=',0)->orderBy('created_at', 'desc')->get();

         return Response::json(['OrderHistory'=>$history]);

    }

    public function ordertrack(Request $request){
        $id=Auth::user()->id;
        $order_id=$request->order_id;
        $track=order_track::all()->where('user_id','=',$id)->where('order_complete','=',0)->where('order_id','=',$order_id);
        return Response::json(['success'=>$track]);

    }

    public function rescedule(Request $request){
        $id=Auth::user()->id;
        $date=$request->date;
        $time=$request->time;
        $reason=$request->reason;
        $order_id=$request->order_id;

        $update=DB::update('update appoinment set service_date=?, service_time=?,reason_newappoin=? where user_id=? AND status=0 AND order_id=?',[$date,$time,$reason,$id,$order_id]);
        if($update==true){
            return Response::json(['success'=>'update schedule successfully']);
        }else{
             return Response::json(['failed'=>'failed update']);
        }
    }
    /**
     * reschedule order
     */

    // reschedule order date and time
    public function rescheduleOrder(Request $request){
      $userId = Auth::user()->id;
      $service_time = $this->serviceStringToDate($request->date, $request->time);

      $update = appoinment::where(['order_id'=> $request->order_id, 'user_id' =>$userId])
                ->update(['service_date'=> $request->date, 'service_time' => $service_time]);

        if($update==true){
          return Response::json(['success'=>$update,'message'=>'update successfully']);
      }else{
            return Response::json(['message'=>'check order_id'],404);
      }
    }
    // string month time to date
    public function serviceStringToDate($service_date, $service_time)
    {
        Log::info($service_date);
      $service_time = explode('-', $service_time);
    //   $service_date = preg_split('/(?<=[0-9])(?=[a-z]+)/i',$service_date);
    $service_date = explode(' ', $service_date);
      Log::info($service_date);
      $time = date("n",strtotime($service_date[1])).'/'.$service_date[0].'/'.date('Y').' '.$service_time[0];
      $time = strtotime($time);
      $newformat = date('Y-m-d h:i:s ',$time);
      return $newformat;
    }


    public function getModelDetails(Request $request)
    {
      $modelName = $request->modelName;
      $brandModel = brandModel::where('brand_model_name', $modelName)->first();
      return Response::json(array(
          'status' => 'success',
          'modalTitle' => $brandModel['model_title'],
          'modelDiscription' => $brandModel['about']),200);
    }
    
    public function aboutData() {
        $aboutUs = AboutUs::first();
      return Response::json(array(
          'status' => 'success',
          'about' => $aboutUs),200);
    }
// getAllServices
    public function getAllServices()
    {
      $services = Service::where('status', 'active')->get();
      return Response::json(array(
          'status' => 'success',
          'services' => $services->toArray()),200);
    }
    
    // offerApply
    public function offerApply(Request $request)
    {
      $offer = Offer::where(['offer_status' => 'active', 'offer_code' => $request->offer_code])->first();
      if ($offer) {
           $user_id = $request->user_id;
        // Log::info($user_id);
        $orderData = appoinment::where(['user_id' => $user_id, 'offer_id' => $offer->id])->first();
        if ($orderData) {
          return Response::json(array(
              'status' => 'error',
              'message' => 'Already Applied'),200);
        }
        
        return Response::json(
            array(
              'status' => 'success',
              'offer_percentage' => $offer->offer_amount,
              'offer_id' => $offer->id),
              200);
      }else {
        return Response::json(array(
            'status' => 'error',
            'message' => 'Offer Invalid'),200);
      }
    }
    public function emailInvoice($invoiceData, $productDetails)
    {
      /*email code async*/
        // $invoiceData = new OrderInvoiceDTO();

        // $productDetails = array('service_name' => 'screen', 'service_number' => 100001, 'price' => 500);

        //
        // return view('emails.invoice', compact('invoiceData', 'productDetails'));
        // OrderInvoice::dispatch($invoiceData)
        //              ->onQueue('mail_queue')
        //              ->onConnection('database');
        Mail::send('emails.invoice', ['invoiceData' => $invoiceData, 'productDetails' => $productDetails], function ($m) use ($invoiceData) {
                  $m->from( env('MAIL_USERNAME'), env('APP_NAME') );

                  $m->to($invoiceData['email'], $invoiceData['name'])->subject('Order Invoice of '.env('APP_NAME'));
                });
       /*end email code */
    }
    
    // send sms  
    public function sendSMS($mobile, $message='')
    {
      $url = "http://smpp.webtechsolution.co/http-tokenkeyapi.php?authentic-key=35376261636b73746167653030313536341569845354&senderid=SPAZME&route=4&number=$mobile&message=$message";
       $c = curl_init();
       curl_setopt($c,CURLOPT_RETURNTRANSFER,1);
       curl_setopt($c,CURLOPT_HTTPGET ,1);

       curl_setopt($c, CURLOPT_URL, $url);
       $contents = curl_exec($c);
     if (curl_errno($c)) {
       echo 'Curl error: ' . curl_error($c);
     }else{
       curl_close($c);
     }
    }
}
