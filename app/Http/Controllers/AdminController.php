<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\slider;
use DB;
use Redirect;
use App\brandDetails;
use App\User;
use App\brandModel;
use App\brandcolor;
use App\appoinment;
use App\contact;
use App\AboutUs;
use App\AvailableStatus;
use App\termsandcondition;
use App\benifitAbout;
use App\questionanswer;
use Log;
use Mail;

use App\Mail\OrderConfirmation;
use App\Mail\OrderCancel;



class AdminController extends Controller
{
  public function login(Request $request)
    {

      // This section is the only change
      $user = User::where('email', $request->input('email'))->first();

      if ($user) {
          if ($user->status == 'active' && $user->role == 'admin') {
            //   Auth::login($user, $request->has('remember'));
             Log::info($user);
             \Auth::login($user);
              return redirect('/dashboard');
          } else {
              return redirect('login') // Change this to redirect elsewhere
                  ->withInput($request->only('email', 'remember'))
                  ->withErrors([
                      'active' => 'You must be active admin to login.'
                  ]);
          }
      }

      return redirect('login')
          ->withInput($request->only('email', 'remember'))
          ->withErrors([
              'email' => 'You must be active admin to login.',
          ]);
  }
  public function home()
  {
   return view('AdminPanel/Home');
 }

 public function AddImage(Request $request){

  request()->validate([

            // 'images' =>'dimensions:width=100,height=100',
    'images' =>'required|mimes:jpeg,png,jpg,gif,svg|max:5000',

  ], [


  ]);
Log::info($request);

// Get image file
$image = $request->file('images');
// Make a image name based on user name and current timestamp
$name = time();
// Define folder path
$folder = '/images/';
// Make a file path where image will be stored [ folder path + file name + file extension]
$filePath = $name. '.' . $image->getClientOriginalExtension();
// Upload image


$file = $image->storeAs($folder, $name.'.'.$image->getClientOriginalExtension(), 'public');
// $this->uploadOne($image, $folder, 'public', $name);
// Set user profile image path in database to filePath
$slider = $filePath;


  $data = array('image' =>$slider, 'created_at'=>NOW(), 'updated_at' =>NOW());

  $save = DB::table('slider')->insert($data);

  if($save==true){

    return Redirect::back()->with('success', 'Sucessfully insert slider');
  }else{
    return Redirect::back()->with('danger', 'check error');

  }
}

public function showSlider(){

  $data=slider::all();
  return view('AdminPanel/EditSlider',['data'=>$data]);
}

public function EditSlider( Request $request){

 request()->validate([

  'images' =>'required|mimes:jpeg,png,jpg,gif,svg|max:5000',

], [


]);
 $id=request('id');
 $image = $request->file('images');
 $input['imagename'] = time().'.'.$image->getClientOriginalExtension();
 $destinationPath = public_path('/images');
 $image->move($destinationPath, $input['imagename']);

 $slider=$input['imagename'];


 $save = DB::update('update slider set image=? where id=?',[$slider, $id]);

 if($save==true){

  return Redirect::back()->with('success', 'Sucessfully Update slider Image');
}else{
  return Redirect::back()->with('danger', 'check error');

}


}

public function  AddBrand(Request $request){

  request()->validate([

    'BrandLogo' =>'required|mimes:jpeg,png,jpg,gif,svg|max:5000',
  ],[
  ]);

//   $image = $request->file('BrandLogo');
//   $input['imagename'] = time().'.'.$image->getClientOriginalExtension();
//   $destinationPath = public_path('/images');
//   $image->move($destinationPath, $input['imagename']);
//   $brandLogo=$input['imagename'];
// Get image file
$image = $request->file('BrandLogo');
// Make a image name based on user name and current timestamp
$name = time();
// Define folder path
$folder = '/images/';
// Make a file path where image will be stored [ folder path + file name + file extension]
$filePath = $name. '.' . $image->getClientOriginalExtension();
// Upload image
$file = $image->storeAs($folder, $name.'.'.$image->getClientOriginalExtension(), 'public');
// $this->uploadOne($image, $folder, 'public', $name);
// Set user profile image path in database to filePath
$brandLogo=$filePath;

  $data = new brandDetails;
  $data->brand_name=request('BrandName');
  $data->brand_image=$brandLogo;

  $data->brand_title = request('brand_title');
  $data->brand_disc=request('brand_disc');

  $submit=$data->save();

  if($submit==true){

    return Redirect::back()->with('success', 'Sucessfully submit the data');
  }else{
    return Redirect::back()->with('danger','error please check');
  }

}

public function brandModel(Request $request){

  request()->validate([
    'BrandModelImage'=>'required|mimes:jpeg,png,jpg,gif,svg|max:5000',
  ],[

  ]);

//   $image = $request->file('BrandModelImage');
//   $input['imagename'] = time().'.'.$image->getClientOriginalExtension();
//   $destinationPath = public_path('/images');
//   $image->move($destinationPath, $input['imagename']);
//   $brand_image=$input['imagename'];

// Get image file
$image = $request->file('BrandModelImage');
// Make a image name based on user name and current timestamp
$name = time();
// Define folder path
$folder = '/images/';
// Make a file path where image will be stored [ folder path + file name + file extension]
$filePath = $name. '.' . $image->getClientOriginalExtension();
// Upload image
$file = $image->storeAs($folder, $name.'.'.$image->getClientOriginalExtension(), 'public');
// $this->uploadOne($image, $folder, 'public', $name);
// Set user profile image path in database to filePath
$brand_image=$filePath;


  $data= new brandModel;
  $data->brand_id=request('brand_id');
  $data->brand_model_name=request('BrandModel');
  $data->brand_image=$brand_image;
  $data->actualprice=request('actualprice');
//   $data->sellingprice=request('SellingPrice');

  $data->service_name = request('service_name');
  $data->screenproPrice=request('ScreenproPrice');
  $data->about=request('about');
  $data->model_title = request('model_title');
  $submit=$data->save();

  if($submit==true){

    return Redirect::back()->with('success', 'Sucessfully submit the data');
  }else{
    return Redirect::back()->with('danger','error please check');
  }

}

public function brandcolor(Request $request){

  request()->validate([

  ],[


  ]);

  $data= new brandcolor;

  $data->brand_id=request('brand_name');
  $data->brand_model_id=request('brand_model');
  $data->color=request('color');

  $submit=$data->save();

  if($submit==true){

    return Redirect::back()->with('success', 'Sucessfully submit the data');
  }else{
    return Redirect::back()->with('danger','error please check');
  }

}
public function myformAjax($id)
{
  // Log::info($id);
  $data=brandModel::where('brand_id','=',$id)->get();

  return json_encode($data);
}



public function contact()
{
 $data=contact::all();
 return view('/AdminPanel/ContactUs',['data'=>$data]);
}

public function about()
{
  $about = AboutUs::orderBy('created_at','DESC')->limit(1)->first();
  return view('/AdminPanel/AboutUs',compact('about'));
}
public function About12(Request $request)
{

 $data= AboutUs::orderBy('created_at','DESC')->limit(1)->first();
 $data->about=request('about');
 $submit=$data->save();

 if($submit==true){

  return Redirect::back()->with('success', 'Sucessfully submit the data');
}else{
  return Redirect::back()->with('danger','error please check');
}


}

public function showAbout(){

  $show=AboutUs::first();
  return view('AdminPanel/EditAbout',['show'=>$show]);
}

public function editAbout($id){

  $update=AboutUs::find($id)->update(['about' => request('about')]);

//   $update->about=request('about');
//   $submit=$update->save();
  if($update==true){

    return Redirect::back()->with('success', 'Sucessfully update the data');
  }else{
    return Redirect::back()->with('danger','error please check');
  }

}


public function AvailableStatus(Request $request){

  request()->validate([
  ],[
    'image'=>'required|mimes:jpeg,png,jpg,gif,svg|max:5000',

  ]);

//   $image = $request->file('image');
//   $input['imagename'] = time().'.'.$image->getClientOriginalExtension();
//   $destinationPath = public_path('/images');
//   $image->move($destinationPath, $input['imagename']);
//   $brandLogo=$input['imagename'];

// Get image file
$image = $request->file('image');
// Make a image name based on user name and current timestamp
$name = time();
// Define folder path
$folder = '/images/';
// Make a file path where image will be stored [ folder path + file name + file extension]
$filePath = $name. '.' . $image->getClientOriginalExtension();
// Upload image
$file = $image->storeAs($folder, $name.'.'.$image->getClientOriginalExtension(), 'public');
// $this->uploadOne($image, $folder, 'public', $name);
// Set user profile image path in database to filePath
$brandLogo=$filePath;


  $data= new AvailableStatus;

  $data->state=request('state');
  $data->pin=request('pincode');
  $data->image=$brandLogo;
  $data->status=request('status');
  $submit=$data->save();
  if($submit==true){

    return Redirect::back()->with('success', 'Sucessfully submit the data');
  }else{
    return Redirect::back()->with('danger','error please check');
  }

}

public function terms(Request $request){

 request()->validate([
 ],[


 ]);

 $data=new termsandcondition;
 $data->about=request('about');
 $submit=$data->save();
 if($submit==true){

  return Redirect::back()->with('success', 'Sucessfully submit the data');
}else{
  return Redirect::back()->with('danger','error please check');
}
}

public function getTerms(){
  $show = termsandcondition::orderBy('created_at','DESC')->limit(1)->first();
  return view('AdminPanel/termsCondition',['show'=>$show]);
}
public function showterms(){

 $show = termsandcondition::orderBy('created_at','DESC')->limit(1)->first();

 return View('AdminPanel/editTerms',['show'=>$show]);
}

public function editTerms($id){

  $update=termsandcondition::find($id);

  $update->about=request('about');
  $submit=$update->save();
  if($submit==true){

    return Redirect::back()->with('success', 'Sucessfully update the data');
  }else{
    return Redirect::back()->with('danger','error please check');
  }

}

public function benifitabout(Request $request){
  $id = $request->id;

  request()->validate([

  ],[


  ]);

  if(!empty($id)){
   $data = benifitAbout::find($id);
    if($request->file('logo')){
    //   $image = $request->file('logo');
    //   $input['imagename'] = time().'.'.$image->getClientOriginalExtension();
    //   $destinationPath = public_path('/images');
    //   $image->move($destinationPath, $input['imagename']);
    //   $brand_image=$input['imagename'];
    //   $data->image=$brand_image;

    // Get image file
$image = $request->file('logo');
// Make a image name based on user name and current timestamp
$name = time();
// Define folder path
$folder = '/images/';
// Make a file path where image will be stored [ folder path + file name + file extension]
$filePath = $name. '.' . $image->getClientOriginalExtension();
// Upload image
$file = $image->storeAs($folder, $name.'.'.$image->getClientOriginalExtension(), 'public');
// $this->uploadOne($image, $folder, 'public', $name);
// Set user profile image path in database to filePath
$data->image=$filePath;

    }
    $data->title=request('title');
    $data->about=request('about');
    $submit=$data->update();
  }

  else{
    // $image = $request->file('logo');
    // $input['imagename'] = time().'.'.$image->getClientOriginalExtension();
    // $destinationPath = public_path('/images');
    // $image->move($destinationPath, $input['imagename']);
    // $brand_image=$input['imagename'];

    $data=new benifitAbout;
    if($request->file('logo'))
    {
        // Get image file
$image = $request->file('logo');
// Make a image name based on user name and current timestamp
$name = time();
// Define folder path
$folder = '/images/';
// Make a file path where image will be stored [ folder path + file name + file extension]
$filePath = $name. '.' . $image->getClientOriginalExtension();
// Upload image
$file = $image->storeAs($folder, $name.'.'.$image->getClientOriginalExtension(), 'public');
// $this->uploadOne($image, $folder, 'public', $name);
// Set user profile image path in database to filePath
$brand_image=$filePath;
    }
    $data->image=$brand_image;
    $data->title=request('title');
    $data->about=request('about');
    $submit=$data->save();
  }


  if($submit==true){

    return Redirect::back()->with('success', 'Sucessfully submit the data');
  }else{
    return Redirect::back()->with('danger','error please check');
  }

}

public function benifitshow(){

  $data=benifitAbout::all();
  return view('AdminPanel/ShowBenifit',['data'=>$data]);
}

public function shwbenifitshow($id){

  $show=benifitAbout::all()->where('id','=',$id);
  return view('AdminPanel/editbenifit',['show'=>$show]);
}



public function editbenifitshow(){

 request()->validate([
 ],[


 ]);
//  $image = $request->file('logo');
//  $input['imagename'] = time().'.'.$image->getClientOriginalExtension();
//  $destinationPath = public_path('/images');
//  $image->move($destinationPath, $input['imagename']);
//  $brand_image=$input['imagename'];

 $data=benifitabout::find($id);
if($request->file('logo')){
     // Get image file
$image = $request->file('logo');
// Make a image name based on user name and current timestamp
$name = time();
// Define folder path
$folder = '/images/';
// Make a file path where image will be stored [ folder path + file name + file extension]
$filePath = $name. '.' . $image->getClientOriginalExtension();
// Upload image
$file = $image->storeAs($folder, $name.'.'.$image->getClientOriginalExtension(), 'public');
// $this->uploadOne($image, $folder, 'public', $name);
// Set user profile image path in database to filePath

 $data->image==$filePath;
}
 $data->title=request('title');
 $data->about=request('about');
 $submit=$data->save();
 if($submit==true){

  return Redirect::back()->with('success', 'Sucessfully update the data');
}else{
  return Redirect::back()->with('danger','error please check');
}
}

public function Available(Request $request){
    Log::info($request);
 $image = $request->file('image');
 $input['imagename'] = time().'.'.$image->getClientOriginalExtension();
 $destinationPath = public_path('/images');
 $image->move($destinationPath, $input['imagename']);
 $brand_image=$input['imagename'];

 $data=new AvailableStatus;

 $data->state=request('state');
 $data->pin=request('pincode');
 $data->image=$brand_image;
 $data->status=request('status');
 $submit=$data->save();
 if($submit==true){

  return Redirect::back()->with('success', 'Sucessfully submit the data');
}else{
  return Redirect::back()->with('danger','error please check');
}

}

public function showavailable(){

  $show=AvailableStatus::all();
  return view('AdminPanel/ShowAvailablePin',['show'=>$show]);

}



public function questionAnswer(Request $request){

 request()->validate([
 ],[


 ]);
 $data= new questionanswer;
 $data->title=$request->title;
 $data->about=$request->about;
 $data->save();

 return Redirect::back()->with('success','Sucessfully submit data');
}

public function showqa(){
  $show=questionanswer::all();
  return view('AdminPanel/showqa',['show'=>$show]);
}

public function getAllUsers(Request $request){
  if($request->has('user_name') || $request->has('email') || $request->has('mobile'))
    {
      $query = (new User)->newQuery();
        if($request->has('user_name') && $request->input('user_name') != ''){
          $query->where('name', 'Like', '%' . $request->input('user_name') . '%');
        }

        if($request->has('mobile') && $request->input('mobile') != ''){
          $query->where('mobile', 'Like', '%' . $request->input('mobile') . '%');
        }

        if($request->has('email') && $request->input('email') != ''){
          $query->where('email', 'Like', '%' . $request->input('email') . '%');
        }

        $users = $query->orderBy('created_at', "desc")->paginate(10);
    } else {
        $users = User::orderBy('created_at', 'desc')->paginate(10);
    }
  return view('AdminPanel.users-list',compact('users'));
}

public function getAllOrders(Request $request){
  if($request->has('query_name') || $request->has('query_mobile') || $request->has('query_order_status'))
    {
      $query = (new appoinment)->newQuery();
        if($request->has('query_name') && $request->input('query_name') != ''){
          $query->where('username', 'Like', '%' . $request->input('query_name') . '%');
        }

        if($request->has('query_mobile') && $request->input('query_mobile') != ''){
          $query->where('phone', $request->input('query_mobile'));
        }

        if($request->has('query_order_status') && $request->input('query_order_status') != 'all' && $request->input('query_order_status') != ''){
          $query->where('track_point', $request->input('query_order_status') );
        }

        $orders = $query->orderBy('created_at', "desc")->paginate(10);
    } else {
        $orders = $orders=appoinment::orderBy('created_at', 'desc')->paginate(10);
    }
  return view('AdminPanel.all-orders',compact('orders'));
}

public function getPrice(){
  $brands = brandDetails::all();
  $modals = array();
  $i=0;

  foreach($brands as $brand){
    $modal=brandModel::where('brand_id',$brand->id)->get();
    if(!empty($modal)){
      foreach($modal as $mod){
        $modals[$i++] = $mod;
      }
    }
  }

            // foreach($models as $m){
            //     echo $m;
            // }

            // die;
  return view('AdminPanel.Price',compact('modals'));
}

public function changeOrderStatus(Request $request){
    Log::info($request);
    $id = $request->ID;
  $status = $request->status;
  $order = appoinment::find($id);
  $orderData = $order;
  $user = User::where('id', $order['user_id'])->first();
  $order->track_point = $status;
  $order->update();


switch ($status) {
    case 'Confirm_Order':
    // send welcome mail to user
    if ($user && $user['email']) {
    //   OrderConfirmation::dispatch($user)
    //         ->onQueue('mail_queue')
    //         ->onConnection('database');
    Mail::send('emails.confirmOrder', ['user' => $user, 'orderData' => $orderData], function ($m) use ($user) {
        $m->from( env('MAIL_USERNAME'), env('APP_NAME') );

        $m->to($user->email, $user->name)->subject('Order has been confirm '.env('APP_NAME'));
      });
    }
      break;
    case 'Order_cancelled':
    // send welcome mail to user
    if ($user && $user['email']) {
    // OrderCancel::dispatch($user)
    //       ->onQueue('mail_queue')
    //       ->onConnection('database');
    Mail::send('emails.cancelOrder', ['user' => $user, 'orderData' => $orderData], function ($m) use ($user) {
      $m->from( env('MAIL_USERNAME'), env('APP_NAME') );

      $m->to($user->email, $user->name)->subject('Order has been cancelled '.env('APP_NAME'));
    });
          }
      break;

    default:
      // code...
      break;
  }

  return response()->json(['status' => 'Updated Successfully']);
}

  public function getInvoice($id){

        $user=appoinment::find($id);

        return view('AdminPanel/invoice',compact('user'));
        }
  /**
    * Show the application dashboard.
    *
    * @return \Illuminate\Http\Response
    */
   public function deleteAll(Request $request)
   {
       $ids = $request->ids;
       DB::table("users")->whereIn('id', explode(",",$ids))->delete();
       foreach(explode(",",$ids) as $id){
           if(appoinment::where('user_id', '=', $id)->exists())
           appoinment::where('user_id', '=', $id)->update(['track_point', 'user_deleted_by_admin']);
       }

       return response()->json(['success'=>"Products Deleted successfully."]);
   }
}
