<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use StdClass, Response;

class ReviewController extends Controller
{
    public function review_list(Request $request)
    {
    	$reviews=array();
    	$object = new StdClass;
    	$object->name = "Shashank";
    	$object->rating = 4;
    	$object->review = "This is a dummy review which is clearly stating that the feedback is good and service was optimum";

    	array_push($reviews, $object);
    	array_push($reviews, $object);
    	array_push($reviews, $object);
        return Response::json(array(
            'status' => 'success',
            'reviews' => $reviews),
            200
        );
    }
}
