<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use App\OTP;
use JWTFactory;
use JWTAuth;
use Response;
use Tymon\JWTAuth\Exceptions\JWTException;
use Validator, DB, Hash, Illuminate\Support\Facades\Password, StdClass;
use Illuminate\Auth\Events\Registered;
use App\Jobs\SignupMail;
use App\Jobs\SendVerificationEmail;
use Log;
use App\Mail\SendOtp;
use App\Mail\UserRegistration;
use App\Mail\UserRegistrationToAdmin;
use Mail;

class AuthApiController extends Controller
{
    /**
     * API Register
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */


    public function register(Request $request)
    {
        $credentials = $request->only('mobile');
        $mobile = $request->mobile;
            $randphone = rand(100000,999999);
        $user = User::where('mobile', $request->mobile)->first();
        if (!$user){
            $rules = [
                'mobile' => 'required|unique:users',
            ];
            $validator = Validator::make($credentials, $rules);
            if($validator->fails()) {
                return response()->json(['success'=> false, 'error'=> $validator->messages()]);
            }
            $password = str_random($length = 6);
            $password = 'password@32';
            $email_token = base64_encode($request->mobile);
            $user = User::create(['mobile' => $mobile, 'otp' => $randphone, 'email_token' => $email_token, 'password' => Hash::make($password)]);

            $message = "Successfully registered. OTP Sent";

            if($user && $user['email']){
             // send welcome mail to user
                UserRegistration::dispatch($user)
                      ->onQueue('mail_queue')
                      ->onConnection('database');

                // send mail synchronously
                Mail::send('emails.register', ['user' => $user], function ($m) use ($user) {
                  $m->from( env('MAIL_USERNAME'), env('APP_NAME') );

                  $m->to($user->email, $user->name)->subject('Selling Simplified with '.env('APP_NAME'));
                });

        // send mail synchronously
                Mail::send('emails.userRegisterToAdmin', ['user' => $user], function ($m) use ($user) {
                  $m->from( env('MAIL_USERNAME'), env('APP_NAME') );

                  $m->to('contact.spazeme@gmail.com', 'admin')->subject('User registered on '.env('APP_NAME'));
                });
        }

        }else{

            User::where('id', $user->id)->update(['otp' => $randphone ]);
            $user = User::find($user->id);
            $message = "OTP Sent";

        // event(new Registered($user = $this->create($request->all())));
        // dispatch(new SendVerificationEmail($user));
        // $request->email = $request->mobile;
    }

    // ==========================otp ==================================

      if($user && $user['email']){
          // send mail for otp
         // if($user && $user['email']){
         //      SendOtp::dispatch($user)
         //            ->onQueue('mail_queue')
         //            ->onConnection('database');
         // }

             // send mail synchronously
             Mail::send('emails.otp', ['user' => $user], function ($m) use ($user) {
               $m->from( env('MAIL_USERNAME'), env('APP_NAME') );

               $m->to($user->email, $user->name)->subject('OTP for '.env('APP_NAME'));
             });
     }

        if ($mobile){

                $send_message = "Please%20use%20OTP%20$user->otp%20to%20proceed%20with%20your%20transaction.";
                $url = "http://smpp1.webtechsolution.co/http-tokenkeyapi.php?authentic-key=39316261636b73746167653032333637331559059857&senderid=MBIZTL&route=1&number=$mobile&message=$send_message";
                $url = "http://smpp.webtechsolution.co/http-tokenkeyapi.php?authentic-key=35376261636b73746167653030313536341569845354&senderid=SPAZME&route=4&number=$mobile&message=$send_message";

                  $c = curl_init();
                  curl_setopt($c,CURLOPT_RETURNTRANSFER,1);
                  curl_setopt($c,CURLOPT_HTTPGET ,1);

                  curl_setopt($c, CURLOPT_URL, $url);
                  $contents = curl_exec($c);

                if (curl_errno($c)) {
                  echo 'Curl error: ' . curl_error($c);
                }else{
                  curl_close($c);
                }

            }



        return response()->json(['success'=> true, 'message'=> $message]);

}



    public function login(Request $request)
    {
        $rules = [
            'mobile' => 'required',
            'otp' => 'required',
        ];
        $user = User::where('mobile', $request->mobile)->where('otp', $request->otp)->first();

        $credentials = $request->only('mobile','otp');
        $validator = Validator::make($credentials, $rules);
        if($validator->fails()) {
            return response()->json(['success'=> false, 'message'=> "Please validate before sending"], 302);
        }
        try {
            // attempt to verify the credentials and create a token for the user
            if ($user){
                if (!$token = JWTAuth::attempt([
                                                'mobile' => $request->mobile,
                                                'password' => 'password@32'
                                                ])) {
                    return response()->json(['success' => false, 'message' => 'We cant find an account with this credentials.'], 401);
                }
            }
            else
                return response()->json(['success' => false, 'message' => 'We cant find an account with this credentials.'], 401);
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['success' => false, 'message' => 'Failed to login, please try again.'], 500);
        }
        // all good so return the token
        $user = User::where('mobile', $request->mobile)->first();
        return response()->json(['success' => true, 'user'=> [ 'token' => $token, 'user' => $user], 'message' => 'data received successfully'], 200);
    }
    /**
     * Log out
     * Invalidate the token, so user cannot use it anymore
     * They have to relogin to get a new token
     *
     * @param Request $request
     */
    public function logout(Request $request) {
        $this->validate($request, ['token' => 'required']);
        try {
            JWTAuth::invalidate($request->input('token'));
            return response()->json(['success' => true, 'message'=> "You have successfully logged out."]);
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['success' => false, 'error' => 'Failed to logout, please try again.'], 500);
        }
    }


    public function recover(Request $request)
    {
        $user = User::where('email', $request->email)->first();
        if (!$user) {
            $error_message = "Your email address was not found.";
            return response()->json(['success' => false, 'error' => ['email'=> $error_message]], 401);
        }
        try {
            // Password::sendResetLink($request->only('email'), function (Message $message) {
            //     $message->subject('Your Password Reset Link');
            // });
        } catch (\Exception $e) {
            //Return with error
            $error_message = $e->getMessage();
            return response()->json(['success' => false, 'error' => $error_message], 401);
        }
        return response()->json([
            'success' => true, 'data'=> ['message'=> 'A reset email has been sent! Please check your email.']
        ]);
    }

    public function change_password(Request $request)
    {

        $response = new StdClass;
        $status = 400;
        $message = "Password not matched";
        $id = $request->user()->id;
        if (!(Hash::check($request->old_password, $request->user()->password))) {
            // The passwords matches
            $message = "Your current password does not matches with the password you provided. Please try again.";
        }
        if(strcmp($request->old_password, $request->new_password) == 0){
            //Current password and new password are same
            $message = "New Password cannot be same as your current password. Please choose a different password.";
        }
        $validatedData = $request->validate([
        'old_password' => 'required',
        'new_password' => 'required',
        ]);
        //Change Password
        $user = $request->user();
        $user->password = bcrypt($request->get('new_password'));
        $user->save();
        if ($user){
            $status = 200;
            $message = "Password changed successfully";
        }
        $response->status = $status;
        $response->message = $message;

        return response()->json($response);

    }

    public function passowordsendOTP(Request $request)
    {
        $response = new StdClass;
        $status = 400;
        $message = "Something Went Wrong";
        $id = $request->mobile;
        $mobile = $request->mobile;
        $user = User::where('mobile', $id)->first();
        $randphone = rand(100000,999999);
        $user->remember_token = $randphone;
        $user->update();
        $message = "Your%20OTP%20is%20$randphone.%20Please%20use%20this%20otp%20to%20reset%20your%20password.";
        $url = "http://103.16.101.52:8080/sendsms/bulksms?username=bcks-imzhnd&password=Super123&type=0&dlr=1&destination=$mobile&source=BSSPLI&message='$message'";
        $url = "http://smpp.webtechsolution.co/http-tokenkeyapi.php?authentic-key=35376261636b73746167653030313536341569845354&senderid=SPAZME&route=4&number=$mobile&message=$message";
         $c = curl_init();
         curl_setopt($c,CURLOPT_RETURNTRANSFER,1);
         curl_setopt($c,CURLOPT_HTTPGET ,1);

         curl_setopt($c, CURLOPT_URL, $url);
         $contents = curl_exec($c);
           if (curl_errno($c)) {
               Log::info('Curl error: ' . curl_error($c));
             echo 'Curl error: ' . curl_error($c);
           }else{
             curl_close($c);
           }

               // send mail for otp
                   if($user && $user['email']){

                        // SendOtp::dispatch($user)
                        //       ->onQueue('mail_queue')
                        //       ->onConnection('database');
                        // send mail synchronously
                        Mail::send('emails.otp', ['user' => $user], function ($m) use ($user) {
                          $m->from( env('MAIL_USERNAME'), env('APP_NAME') );

                          $m->to($user->email, $user->name)->subject('OTP for '.env('APP_NAME'));
                        });
                   }
       $message = "Sms sent";
       $status = 200;


        $response->status = $status;
        $response->message = $message;
        return response()->json($response);
    }
    public function sendOTP(Request $request)
    {
        $response = new StdClass;
        $status = 400;
        $message = "Something Went Wrong";
        $id = $request->user()->id;
        $user = User::find($id);
        if ($user->mobile_verification != 'Verified'){
            $mobile = $user->mobile;
            if ($mobile){
                $randphone = rand(100000,999999);
                $otp = new OTP;
                $otp->user_id = $id;
                $otp->otp = $randphone;
                $otp->save();
                $message = "Your%20OTP%20is%20$randphone.%20Please%20use%20this%20otp%20to%20verify%20your%20account.";
                $url = "http://smpp.webtechsolution.co/http-tokenkeyapi.php?authentic-key=35376261636b73746167653030313536341569845354&senderid=SPAZME&route=4&number=$mobile&message='$message'";
                 $c = curl_init();
                 curl_setopt($c,CURLOPT_RETURNTRANSFER,1);
                 curl_setopt($c,CURLOPT_HTTPGET ,1);

                 curl_setopt($c, CURLOPT_URL, $url);
                 $contents = curl_exec($c);
                   if (curl_errno($c)) {
                       // Log::info('Curl error: ' . curl_error($c));
                     echo 'Curl error: ' . curl_error($c);
                   }else{
                     curl_close($c);
                   }

                       // send mail for otp
                   if($user && $user['email']){
                        // SendOtp::dispatch($user)
                        //       ->onQueue('mail_queue')
                        //       ->onConnection('database');
                        // send mail synchronously
                        Mail::send('emails.otp', ['user' => $user], function ($m) use ($user) {
                          $m->from( env('MAIL_USERNAME'), env('APP_NAME') );

                          $m->to($user->email, $user->name)->subject('OTP for '.env('APP_NAME'));
                        });
                   }


                   $message = "Sms sent";
                   $status = 200;

            }
            else {
                $message = "No number found";
                $status = 200;
            }

        }
        else {
            $message = "Already Verified";
            $status = 200;
        }
        $response->status = $status;
        $response->message = $message;
        return response()->json($response);
    }

    public function validateOTP(Request $request)
    {
        $response = new StdClass;
        $status = 400;
        $message = "Something Went Wrong";
        $id = $request->user()->id;
        $user = User::find($id);
        if ($user){
            $otp = OTP::where('user_id', $id)->orderBy('id', 'DESC')->first();
            if (isset($request->otp) && isset($otp->otp) && $request->otp == $otp->otp){
                $user->mobile_verification = "Verified";
                $user->update();
                $status = 200;
                $message = "Otp verified";

            }
            else {
                $status = 200;
                $message = "Otp Missmatch";
            }
        }
        else {
            $status = 200;
            $message = "User Not Found";
        }
        $response->status = $status;
        $response->message = $message;
        return response()->json($response);
    }

    public function validatechangeOTP(Request $request)
    {
        $response = new StdClass;
        $status = 400;
        $message = "Something Went Wrong";
        $id = $request->mobile;
        $user = User::where('mobile', $id)->first();
        if ($user){
            if ($user->remember_token == $request->otp){
                $user->password = Hash::make($request->password);
                $user->update();
                $status = 200;
                $message = "Password Changed";

            }
            else {
                $message = "Otp Missmatch";
            }
        }
        else {
            $message = "User Not Found";
        }
        $response->status = $status;
        $response->message = $message;
        return response()->json($response);
    }

}
