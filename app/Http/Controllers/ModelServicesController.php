<?php

namespace App\Http\Controllers;

use App\ModelServices;
use App\brandModel;
use App\Service;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use DB;
use Log;

class ModelServicesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      $serviceId = $request->serviceId;
      $serviceName = Service::where('id', $serviceId)->first()->service_name;

      if($serviceId){

        $query = DB::table('model_services')
                            ->join('brand_model', 'brand_model.id', '=', 'model_services.model_id')
                            ->where('model_services.service_id', $serviceId)
                            ->select(
                              'model_services.id as model_services_id',
                              'model_services.actual_price as actual_price',
                              'model_services.our_price as our_price',
                              'model_services.status as status',
                              'brand_model.*');

        $order_by_column = 'created_at';
        $order_by_value = 'desc';
        if($request->has('service_name') || $request->has('service_status') || $request->has('order_by_column') || $request->has('order_by_value'))
          {
              if($request->has('service_name') && $request->input('service_name') != ''){
                $query->where('brand_model.brand_model_name', 'Like', '%' . $request->input('service_name') . '%');
              }

              if($request->has('service_status') && $request->input('service_status') != 'all' && $request->input('service_status') != ''){
                $query->where('model_services.status', $request->input('service_status') );
              }

              if($request->has('order_by_column') && $request->input('order_by_column') != 'all' && $request->input('order_by_column') != ''){
                $order_by_column = $request->input('order_by_column');
              }

              if($request->has('order_by_value') && $request->input('order_by_value') != 'all' && $request->input('order_by_value') != ''){
                $order_by_value = $request->input('order_by_value');
              }

              $modelServices = $query->orderBy($order_by_column, $order_by_value)->paginate(20);
          } else {
            $modelServices = $query->orderBy('created_at', 'desc')->paginate(20);
          }

      }else {
        return redirect('services');
      }
      Log::info($modelServices);

      return view('AdminPanel.modelservices.index', compact('modelServices', 'serviceId', 'serviceName'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
      $serviceId = $request->serviceId;
      // Log::info($serviceId);
      $modelData = brandModel::select('id', 'brand_model_name')->get();
      // Log::info($modelData);
      return view('AdminPanel.modelservices.create', compact('modelData', 'serviceId' ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      // Log::info($request);
      $validator = Validator::make($request->toArray(), [
        'service_id'=>'required',
        // 'model_id'=>'required|unique:model_services',
        'model_id'=>'required',
        'actual_price'=>'required|numeric',
        'our_price'=>'required|numeric',
        'status'=>'required',
      ]);

      if((count($validator->messages()) > 0) || ($validator->fails()))
         {
           return redirect()->back()->withErrors($validator->errors())->withInput($request->toArray());
         }
         ModelServices::create([
           'service_id' => $request->service_id,
           'model_id' => $request->model_id,
           'actual_price' => $request->actual_price,
           'our_price' => $request->our_price,
           'status' => $request->status
         ]);

         return redirect('model-services?serviceId='.$request->service_id)->with('success', 'Sucessfully created the data');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ModelServices  $modelServices
     * @return \Illuminate\Http\Response
     */
    public function show(ModelServices $modelServices)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ModelServices  $modelServices
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $modelServiceId)
    {
      $serviceId = $request->serviceId;
      $modelServices = ModelServices::where('id', $modelServiceId)->first();
      // Log::info($modelServiceId);
      // Log::info($modelServices);
      $modelData = brandModel::select('id', 'brand_model_name')->get();
        return view('AdminPanel.modelservices.edit', compact('modelServices', 'serviceId', 'modelData'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ModelServices  $modelServices
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $modelServiceId)
    {
      // Log::info($request);
      $validator = Validator::make($request->toArray(), [
        'service_id'=>'required',
        // 'model_id'=>'required|unique:model_services,model_id,'.$modelServices->id,
        'actual_price'=>'required|numeric',
        'our_price'=>'required|numeric',
        'status'=>'required',
      ]);

      if((count($validator->messages()) > 0) || ($validator->fails()))
         {
           return redirect()->back()->withErrors($validator->errors())->withInput($request->toArray());
         }
         {
           $modelServices = ModelServices::where('id', $modelServiceId)->first();
           ModelServices::find($modelServices->id)->update(['actual_price' => $request->actual_price, 'our_price' => $request->our_price, 'status' => $request->status]);
         }

      return redirect('model-services?serviceId='.$request->service_id)->with('success','Model Service  updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ModelServices  $modelServices
     * @return \Illuminate\Http\Response
     */
    public function destroy(ModelServices $modelServices)
    {
        //
    }
}
