<?php

namespace App\Http\Controllers;

use App\Service;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Log;


class ServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      $order_by_column = 'created_at';
      $order_by_value = 'desc';
      if($request->has('service_name') || $request->has('service_status') || $request->has('order_by_column') || $request->has('order_by_value'))
        {
          $query = (new Service)->newQuery();
            if($request->has('service_name') && $request->input('service_name') != ''){
              $query->where('service_name', 'Like', '%' . $request->input('service_name') . '%');
            }

            if($request->has('service_status') && $request->input('service_status') != 'all' && $request->input('service_status') != ''){
              $query->where('status', $request->input('service_status') );
            }

            if($request->has('order_by_column') && $request->input('order_by_column') != 'all' && $request->input('order_by_column') != ''){
              $order_by_column = $request->input('order_by_column');
            }

            if($request->has('order_by_value') && $request->input('order_by_value') != 'all' && $request->input('order_by_value') != ''){
              $order_by_value = $request->input('order_by_value');
            }

            $services = $query->orderBy($order_by_column, $order_by_value)->paginate(10);
        } else {
            $services = Service::orderBy('created_at', 'desc')->paginate(10);
        }
        return view('AdminPanel.services.index', compact('services'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('AdminPanel.services.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      // Log::info($request);
      $validator = Validator::make($request->toArray(), [
        'service_name'=>'required',
        'service_image'=>'required|mimes:jpeg,png,jpg,gif,svg|max:2000',
        'status'=>'required',
      ]);

      if((count($validator->messages()) > 0) || ($validator->fails()))
         {
           return redirect()->back()->withErrors($validator->errors())->withInput($request->toArray());
         }
         if ($request->file('service_image')) {
             // Get image file
             $image = $request->file('service_image');
             $service_image =$this->imageUpload($image);
         }

         Service::create(['service_name' => $request->service_name, 'status' => $request->status, 'service_image' => $service_image]);

         return redirect('services')->with('success', 'Sucessfully update the data');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function show(Service $service)
    {
        // Log::info($service);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function edit(Service $service)
    {
        // Log::info($service);
        return view('AdminPanel.services.edit', compact('service'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Service $service)
    {
        // Log::info($service);
        // Log::info($request);
        $validator = Validator::make($request->toArray(), [
          'service_name'=>'required',
          'service_image'=>'mimes:jpeg,png,jpg,gif,svg|max:2000',
          'status'=>'required',
        ]);

        if((count($validator->messages()) > 0) || ($validator->fails()))
           {
             return redirect()->back()->withErrors($validator->errors())->withInput($request->toArray());
           }

           if ($request->file('service_image')) {
               // Get image file
               $image = $request->file('service_image');
               $service_image =$this->imageUpload($image);
               Service::find($service->id)->update(['service_name' => $request->service_name, 'status' => $request->status, 'service_image' => $service_image]);
           }else {
             Service::find($service->id)->update(['service_name' => $request->service_name, 'status' => $request->status]);
           }

        return redirect('services')->with('success','Service updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $service)
    {
      $delete = false;
      if (request('id')) {
        $delete = Service::where('id', '=', request('id'))->delete();
      }
  		if($delete==true){
  		 return redirect()->back()->with('success', 'Data Deleted Sucessfully');
  		}else{
  		 return redirect()->back()->with('danger','error please check');
  		}
    }

    /**
     * save image
     *
     * @param image file
     * @return uploaded image name
     */
     public function imageUpload($image)
     {
       // Make a image name based on user name and current timestamp
       $name = time();
       // Define folder path
       $folder = '/images/';
       // Make a file path where image will be stored [ folder path + file name + file extension]
       $filePath = $name. '.' . $image->getClientOriginalExtension();

        // Log::info($filePath);
       // Upload image
       $file = $image->storeAs($folder, $name.'.'.$image->getClientOriginalExtension(), 'public');
       // $this->uploadOne($image, $folder, 'public', $name);
       // Set user profile image path in database to filePath
       return $filePath;
     }
}
