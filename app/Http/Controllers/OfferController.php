<?php

namespace App\Http\Controllers;

use App\Offer;
use App\brandDetails;
use App\brandModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Log;


class OfferController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $offers = Offer::all();
      return view('AdminPanel.offer.index', compact('offers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $brands = brandDetails::orderBy('brand_name', 'asc')->get();
      $models = brandModel::orderBy('brand_model_name', 'asc')->get();
      return view('AdminPanel.offer.create', compact('brands', 'models'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      // Log::info($request);
      $validator = Validator::make($request->toArray(), [
        'offer_name'=>'required',
        'offer_code'=>'required',
        'offer_type'=>'required',
        'offer_amount'=>'required',
        'offer_status'=>'required',
      ]);

      if((count($validator->messages()) > 0) || ($validator->fails()))
       {
         return redirect()->back()->withErrors($validator->errors())->withInput($request->toArray());
       }
       $offer_on = 'all';
       if($request->offer_type == 'brand'){
          $offer_on =  $request->brand;
       }

       if($request->offer_type == 'model'){
          $offer_on =  $request->model;
       }
         Offer::create([
           'offer_name' => $request->offer_name,
           'offer_code' => $request->offer_code,
           'offer_type' => $request->offer_type,
           'offer_on' => $offer_on,
           'offer_amount' => $request->offer_amount,
           'offer_status' => $request->offer_status,
         ]);

         return redirect('offers')->with('success', 'Sucessfully update the data');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Offer  $offer
     * @return \Illuminate\Http\Response
     */
    public function show(Offer $offer)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Offer  $offer
     * @return \Illuminate\Http\Response
     */
    public function edit(Offer $offer)
    {
      $brands = brandDetails::orderBy('brand_name', 'asc')->get();
      $models = brandModel::orderBy('brand_model_name', 'asc')->get();
      return view('AdminPanel.offer.edit', compact('offer', 'brands', 'models'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Offer  $offer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Offer $offer)
    {
      $validator = Validator::make($request->toArray(), [
        'offer_name'=>'required',
        'offer_code'=>'required',
        'offer_type'=>'required',
        'offer_amount'=>'required',
        'offer_status'=>'required',
      ]);

      if((count($validator->messages()) > 0) || ($validator->fails()))
       {
         return redirect()->back()->withErrors($validator->errors())->withInput($request->toArray());
       }
       $offer_on = 'all';
       if($request->offer_type == 'brand'){
          $offer_on =  $request->brand_id;
       }

       if($request->offer_type == 'model'){
          $offer_on =  $request->model_id;
       }
         Offer::find($offer->id)->update([
           'offer_name' => $request->offer_name,
           'offer_code' => $request->offer_code,
           'offer_type' => $request->offer_type,
           'offer_on' => $offer_on,
           'offer_amount' => $request->offer_amount,
           'offer_status' => $request->offer_status,
         ]);

         return redirect('offers')->with('success', 'Sucessfully update the data');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Offer  $offer
     * @return \Illuminate\Http\Response
     */
     public function destroy(Request $request)
     {
       $delete = false;
       if (request('id')) {
         $delete = Offer::where('id', '=', request('id'))->delete();
       }
   		if($delete==true){
   		 return redirect()->back()->with('success', 'Data Deleted Sucessfully');
   		}else{
   		 return redirect()->back()->with('danger','error please check');
   		}
     }
}
