<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\slider;
use DB;
use Redirect;
use App\brandDetails;
use App\brandModel;
use App\brandcolor;
use App\contact;
use App\AboutUs;
use App\appoinment;
use App\AvailableStatus;
use App\termsandcondition;
use App\User;
use Log;

class AdminEditController extends Controller
{
	public function showbrandDetails(Request $request){
		$order_by_column = 'created_at';
		$order_by_value = 'desc';
		if($request->has('brand_name') || $request->has('service_status') || $request->has('order_by_column') || $request->has('order_by_value'))
			{
				$query = (new brandDetails)->newQuery();
					if($request->has('brand_name') && $request->input('brand_name') != ''){
						$query->where('brand_name', 'Like', '%' . $request->input('brand_name') . '%');
					}

					if($request->has('service_status') && $request->input('service_status') != 'all' && $request->input('service_status') != ''){
						$query->where('status', $request->input('service_status') );
					}

					if($request->has('order_by_column') && $request->input('order_by_column') != 'all' && $request->input('order_by_column') != ''){
						$order_by_column = $request->input('order_by_column');
					}

					if($request->has('order_by_value') && $request->input('order_by_value') != 'all' && $request->input('order_by_value') != ''){
						$order_by_value = $request->input('order_by_value');
					}

					$services = $query->orderBy($order_by_column, $order_by_value)->paginate(10);
			} else {
					$services = brandDetails::orderBy('created_at', 'desc')->paginate(10);
			}

		// $show=brandDetails::orderBy('brand_name', 'asc')->get();
		return view('/AdminPanel/editHome',['show'=>$services]);
	}
    public function editbrandDetails(Request $request){
        $id=request('id');
    	$data=brandDetails::find($id);
    	$data->brand_name=request('brand_name');
    	$data->brand_title=request('brand_title');
    	$data->brand_disc=request('brand_disc');

			if ($request->has('images')) {
				request()->validate([
	            'images' =>'mimes:jpeg,png,jpg,gif,svg|max:5000',
	             ], [
	            ]);

				$image = $request->file('images');
				$input['imagename'] = time().'.'.$image->getClientOriginalExtension();
				$destinationPath = public_path('/images');
				$image->move($destinationPath, $input['imagename']);
				$brand_image=$input['imagename'];
				$data->brand_image=$brand_image;
			}

    	$save=$data->save();
    	if($save==true){

                    return Redirect::back()->with('success', 'Sucessfully update slider');
                }else{
                    return Redirect::back()->with('danger', 'error');

                }

    }

    	public function editStateDetails(Request $request){
        $id=request('id');
    	$data=AvailableStatus::find($id);
    	$data->status = request('status');

    	$save=$data->save();
    	if($save==true)
			{
            return Redirect::back()->with('success', 'Sucessfully update slider');
        }else{
            return Redirect::back()->with('danger', 'error');

        }

    }

    public function showModel(Request $request){
			$order_by_column = 'created_at';
			$order_by_value = 'desc';
			if($request->has('brand_model_name') || $request->has('service_status') || $request->has('order_by_column') || $request->has('order_by_value'))
				{
					$query = (new brandModel)->newQuery();

						// if($request->has('brand_name') && $request->input('brand_name') != ''){
						// 	$query->where('brand_name', 'Like', '%' . $request->input('brand_name') . '%');
						// }

						if($request->has('brand_model_name') && $request->input('brand_model_name') != ''){
							$query->where('brand_model_name', 'Like', '%' . $request->input('brand_model_name') . '%');
						}

						if($request->has('service_status') && $request->input('service_status') != 'all' && $request->input('service_status') != ''){
							$query->where('status', $request->input('service_status') );
						}

						if($request->has('order_by_column') && $request->input('order_by_column') != 'all' && $request->input('order_by_column') != ''){
							$order_by_column = $request->input('order_by_column');
						}

						if($request->has('order_by_value') && $request->input('order_by_value') != 'all' && $request->input('order_by_value') != ''){
							$order_by_value = $request->input('order_by_value');
						}

						$services = $query->orderBy($order_by_column, $order_by_value)->paginate(10);
				} else {
						$services = brandModel::orderBy('created_at', 'desc')->paginate(10);
				}
    	// $show=brandModel::orderBy('brand_model_name', 'asc')->get();
		return view('/AdminPanel/editModel',['show'=>$services]);


    }
    public function EditModelPage($id){
    	$show=brandModel::all()->where('id','=',$id);
		return view('/AdminPanel/editModelPage',['show'=>$show]);


    }

    public function editbrandModel(Request $request){

    	 $id=request('id');
                $data=brandModel::find($id);

                if($request->file('BrandModelImage')){
                  request()->validate([
                       'BrandModelImage'=>'required|mimes:jpeg,png,jpg,gif,svg|max:5000',
                   ],[

                   ]);

                   $image = $request->file('BrandModelImage');
                   $input['imagename'] = time().'.'.$image->getClientOriginalExtension();
                   $destinationPath = public_path('/images');
                   $image->move($destinationPath, $input['imagename']);
                   $brand_image=$input['imagename'];

                   $data->brand_image=$brand_image;
                }

                $data->brand_id=request('brand_id');
                $data->brand_model_name=request('BrandModel');
                $data->actualprice=request('actualprice');
                // $data->sellingprice=request('SellingPrice');

				$data->service_name = request('service_name');
                $data->screenproPrice=request('ScreenproPrice');
                $data->model_title=request('model_title');
                $data->about=request('about');
                 $submit=$data->save();

       if($submit==true){

        return Redirect::back()->with('success', 'Sucessfully submit the data');
       }else{
        return Redirect::back()->with('danger','error please check');
       }
    }

  public function brandcolor(Request $request){
			$order_by_column = 'created_at';
			$order_by_value = 'desc';
			if($request->has('color_name') || $request->has('service_status') || $request->has('order_by_column') || $request->has('order_by_value'))
				{
					$query = (new brandcolor)->newQuery();

						// if($request->has('brand_name') && $request->input('brand_name') != ''){
						// 	$query->where('brand_name', 'Like', '%' . $request->input('brand_name') . '%');
						// }

						// if($request->has('brand_model_name') && $request->input('brand_model_name') != ''){
						// 	$query->where('brand_model_name', 'Like', '%' . $request->input('brand_model_name') . '%');
						// }

						if($request->has('color_name') && $request->input('color_name') != ''){
							$query->where('color', 'Like', '%' . $request->input('color_name') . '%');
						}

						if($request->has('service_status') && $request->input('service_status') != 'all' && $request->input('service_status') != ''){
							$query->where('status', $request->input('service_status') );
						}

						if($request->has('order_by_column') && $request->input('order_by_column') != 'all' && $request->input('order_by_column') != ''){
							$order_by_column = $request->input('order_by_column');
						}

						if($request->has('order_by_value') && $request->input('order_by_value') != 'all' && $request->input('order_by_value') != ''){
							$order_by_value = $request->input('order_by_value');
						}

						$services = $query->orderBy($order_by_column, $order_by_value)->paginate(10);
				} else {
						$services = brandcolor::orderBy('created_at', 'desc')->paginate(10);
				}
    		// $show=brandcolor::orderBy('color', 'asc')->get();
			foreach ($services as $value) {
				$data = \DB::table('brand_details')->where('id', $value->brand_id)->first();
				if($data)
				$value['brandName'] = $data->brand_name;
				$modelData = \DB::table('brand_model')->where('id', $value->brand_model_id)->first();
				if ($modelData) {
					$value['brandModelName'] = $modelData->brand_model_name;
				}


			}
		return view('/AdminPanel/editcolor',['show'=>$services]);


    }

    public function editbrandcolor(Request $request){


    	request()->validate([

        ],[


        ]);

    	$id=request('id');
        $data=brandcolor::find($id);

        // $data->brand_id=request('brand_name');
        // $data->brand_model_id=request('brand_model');
        $data->color=request('color');

         $submit=$data->save();

       if($submit==true){

        return Redirect::back()->with('success', 'Sucessfully submit the data');
       }else{
        return Redirect::back()->with('danger','error please check');
       }

    }

public function deletebrandcolor(Request $request)
		{
			$delete = brandcolor::where('id', '=', request('id'))->delete();

		if($delete==true){

		 return Redirect::back()->with('success', 'Data Deleted Sucessfully');
		}else{
		 return Redirect::back()->with('danger','error please check');
		}

		}

		public function deleteorder(Request $request)
		{
			$delete = appoinment::where('id', '=', request('id'))->delete();

		if($delete==true){

		 return Redirect::back()->with('success', 'Data Deleted Sucessfully');
		}else{
		 return Redirect::back()->with('danger','error please check');
		}

		}


		public function deleteUser(Request $request)
		{
			$delete = User::where('id', '=', request('id'))->delete();


		if($delete==true){
		    // appoinment::where('user_id', '=', request('id'))->update(['track_point', 'user_deleted_by_admin']);
		 return Redirect::back()->with('success', 'Data Deleted Sucessfully');
		}else{
		 return Redirect::back()->with('danger','error please check');
		}

		}

     public function editTerms(Request $request){

            // Log::info($request);
             $id=request('id');

             $data=termsandcondition::find($id);
             if($data == null){
                 $data = new termsandcondition();
             }

             $data->about=$request->input('about');
             $submit=$data->save();
            if($submit==true){

        return Redirect::back()->with('success', 'Sucessfully update the data');
       }else{
        return Redirect::back()->with('danger','error please check');
       }


}

}
