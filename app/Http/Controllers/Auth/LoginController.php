<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Log;
use App\User;
use Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

      public function login(Request $request)
    {
      // This section is the only change
      $user = User::where('email', $request->input('email'))->first();
      if ($user) {
          if ($user->status == 'active' && $user->role == 'admin') {
              Auth::login($user, $request->has('remember'));
               $user=Auth::user();
               $val = session();
               return redirect()->intended($this->redirectTo);
            //   return view('AdminPanel.Dashboard');
          } else {
              return redirect('login') // Change this to redirect elsewhere
                  ->withInput($request->only('email', 'remember'))
                  ->withErrors([
                      'email' => 'You must be active to login.'
                  ]);
          }
      }

      return redirect('login')
          ->withInput($request->only('email', 'remember'))
          ->withErrors([
              'email' => 'You must be active to login.',
          ]);
  }

  protected function authenticated(Request $request)
    {
        return redirect('/home'); //put your redirect url here
    }
}
