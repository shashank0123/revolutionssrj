<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use StdClass, Response;
use App\brandModel;
use App\Service;
use DB;
use Log;

class ItemPriceController extends Controller
{
    public function estimated_price(Request $request)
    {
    	$brand_name = $request->brand_name;
    	$model_name = $request->model_name;
    	$location = $request->location;
    	$token 		= $request->token;
    	$status = 402;
      // get available services
     $brandModel = brandModel::where('brand_model_name', $model_name)->first();
    //  Log::info($request);
      if($request->service && $request->service != 'null'){
        $model_services = DB::table('services')
                                ->join('model_services','services.id' , '=', 'model_services.service_id' )
                                ->join('brand_model','model_services.model_id' , '=', 'brand_model.id' )
                                ->where('services.service_name', $request->service)
                                ->where('services.status', 'active')
                                ->where('model_services.status', 'active')
                                ->where('model_services.model_id', $brandModel['id'])
                                ->select('brand_model.id as  model_id', 'brand_model.brand_model_name as  modelName', 'services.service_name as serviceName',
                                 'services.service_image as serviceImage','model_services.actual_price as actualprice','model_services.our_price as screenproPrice')
                                ->get();
      }else {
        $model_services = DB::table('services')
                                ->join('model_services','services.id' , '=', 'model_services.service_id' )
                                ->join('brand_model','model_services.model_id' , '=', 'brand_model.id' )
                                // ->where('brand_model.brand_model_name', $model_name)
                                ->where('services.status', 'active')
                                ->where('model_services.status', 'active')
                                ->where('model_services.model_id', $brandModel['id'])
                                ->select('brand_model.id as  model_id', 'brand_model.brand_model_name as  modelName', 'services.service_name as serviceName',
                                 'services.service_image as serviceImage','model_services.actual_price as actualprice','model_services.our_price as screenproPrice')
                                ->get();
      }
    //   $model_services = Service::where('status', 'active')->get();
    // $model_services = DB::table('services')
    //                         ->join('brand_model','services.id' , '=', 'brand_model.service_name' )
    //                         ->where('brand_model.brand_model_name', $model_name)
    //                         ->where('services.status', 'active')
    //                         ->select('brand_model.id as  model_id', 'brand_model.brand_model_name as  modelName', 'services.service_name as serviceName',
    //                          'services.service_image as serviceImage','brand_model.actualprice','brand_model.screenproPrice')
    //                         ->get();
    	if (!$brand_name || !$model_name || !$token || !$location){
    		return Response::json(array(
            'status' => 'failed'),
            400
        );


    	}
    	else{

        $brandModel = brandModel::where('brand_model_name', $model_name)->first();
        if ($brandModel) {
          $cart = array(
  	    					'market_price' => 'RS '.$brandModel['actualprice'],
  	    					'discount' => 'RS '.($brandModel['actualprice'] - $brandModel['screenproPrice']),
  	    					'our_price' => 'RS '.$brandModel['screenproPrice'],
  	    					'total' => 'RS '.($brandModel['screenproPrice']),
  	    					);
        } else {
          $cart = array(
  	    					'market_price' => 'RS 10000',
  	    					'discount' => 'RS 100',
  	    					'our_price' => 'RS 9900',
  	    					'total' => 'RS 9900',
  	    					);
        }

	    	$stats = array(
	    					'screens_repaired' => '200+',
	    					'rated' => '4.1+',
				    );

	    	$device = array(
	    					'device' => $model_name." (".$brand_name.")",
	    					'location' => 'Delhi',
				    );

	    	return Response::json(array(
            'status' => 'success',
            'cart' => $cart,
            'stats' => $stats,
            'device' => $device,
            'model_image' => $brandModel['brand_image'],
            'services' => $model_services
	        ),
            200
        );


    	}
    }
}
