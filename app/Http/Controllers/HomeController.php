<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User; 
use Illuminate\Support\Facades\Auth; 
use Illuminate\Support\Facades\Input;
use Artisan;
use Response;
use Validator;
use Log;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        // Log::info('Auth::user');
        return view('AdminPanel.Dashboard');
    }

     public function register(Request $request) 
    { 
        
        request()->validate([


        ],[

        ]);
        $otp = rand(100000, 999999);
        $otp_at=date('Y-m-d H:m:s');
        $phone=request('phone');
        $time=NOW();
        $data = array('mobile' =>$phone, 'otp'=>$otp, 'otp_at'=>$otp_at,'created_at'=>$time, 'updated_at'=>$time);
        
        $submit=DB::table('users')->insert($data);

        $success['token'] =  $submit->createToken('MyApp')-> accessToken; 
                    $success['phone'] =  $submit->phone;
            return response()->json(['success'=>$success], $this-> successStatus); 
}

// schedule cron job 
    public function runCron () {
        // if (Input::get('type') == 'pairing') {
        //     Artisan::call('bonus:pairing');
        // } else if (Input::get('type') == 'checkGroup') {
        //     Artisan::call('member:group');
        // } else if (Input::get('type') == 'group') {
        //     Artisan::call('bonus:group');
        // } else if (Input::get('type') == 'freeze') {
        //     Artisan::call('shares:freeze');
        // } else if (Input::get('type') == 'direct') {
        //     Artisan::call('bonus:direct');
        // }
        
        // Artisan::call('schedule:run > /dev/null 2>&1');
        Artisan::call('queue:work --queue=mail_queue');

        return Response::json([
            'type' => 'success',
            'message' => 'Job completed successfully'
        ]);
    }
}
