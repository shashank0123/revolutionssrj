<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AvailableStatus extends Model
{
    protected $table='_state_available';
    protected $fillable = ['state', 'pin','image', 'status'];
    
    //   public function getImageUrlAttribute()
    //   {
    //       return public_path().'/images/' . $this->image;
    //     } 
}
