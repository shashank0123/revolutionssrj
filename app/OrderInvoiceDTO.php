<?php

namespace App;

class OrderInvoiceDTO
{
  public $logo;
  public $brand;
  public $model;
  public $color;
  public $name;
  public $email;
  public $total_price;
  public $address;
  public $productDetails;
}

?>
