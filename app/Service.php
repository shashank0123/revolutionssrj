<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Service extends Model
{
    protected $fillable = ['service_name','service_image' , 'status'];

    public function getCreatedAtAttribute($value)
      {
          $date = Carbon::parse($value);
          return $date->format('d-M-Y');
      }

      public function getImageUrlAttribute()
      {
        return 'images/' . $this->service_image;
      }
}
