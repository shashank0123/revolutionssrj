<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class order_track extends Model
{
    protected $table='track_order';
    protected $fillable=['order_id','user_id','placed_order','order_confirmed',	'agent_assign','order_complete','created_at','updated_at'
];
}
