<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class slider extends Model
{
    protected $table='slider';

    protected $fillable =['image'];
    
    public function getImageUrlAttribute()
    {
      return public_path().'/images/' . $this->image;
    }
}
