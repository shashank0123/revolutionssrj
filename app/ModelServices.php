<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModelServices extends Model
{
    //
    protected $fillable = ['model_id', 'service_id', 'actual_price', 'our_price', 'status'];

}
