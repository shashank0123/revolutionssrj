<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class questionanswer extends Model
{
    protected $table='questionanswer';

    protected $fillable=['title','about'];
}
