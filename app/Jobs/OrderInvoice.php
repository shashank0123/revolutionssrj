<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;
use App\OrderInvoiceDTO;

class OrderInvoice implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

      protected $orderInvoiceData;
      /**
       * Create a new job instance.
       *
       * @return void
       */
      public function __construct(OrderInvoiceDTO $orderInvoiceDTO)
      {
          $this->orderInvoiceData = $orderInvoiceDTO;
      }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
      Mail::send('emails.invoice',
        array('orderInvoiceData' => $this->orderInvoiceData),
         function($message){
          $subject = "Order Invoice";
           $message->to($this->orderInvoiceData->email)
           ->subject($subject);
          });
    }
}
