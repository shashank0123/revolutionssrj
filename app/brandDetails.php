<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class brandDetails extends Model
{
    protected $table='brand_details';

    protected $fillable =['brand_name','brand_image', 'brand_disc', 'brand_title'];

    
}
