<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Testimonial extends Model
{
  protected $fillable = ['image_url','client_name','rating','review','status','city','slug'];

  public function getCreatedAtAttribute($value)
    {
        $date = Carbon::parse($value);
        return $date->format('d-M-Y');
    }

    public function getImageUrlAttribute($value)
    {
      return 'images/' . $value;
    }

}
