<?php
use App\brandDetails;
use App\brandModel;
?>

@extends('layouts.Dashboard')

@section('content')

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
	<div class="container">
	    		

     <div>
         <button style="text-align: left;display:inline-block;" class="btn btn-primary delete_all" data-url="{{ url('userDeleteAll') }}">Delete All Selected</button>
     <div style="text-align: center;display:inline-block;    margin-left: 1rem;" ><h3><u><b>Users List</b></u></h3></div>
     </div>
     
     <div class="row mb-rem-1 search-content-row" >
       <form>
         <div class="col-md-3">
           <input type="text" class="form-control mb-2" value="{{request('user_name')}}" autocomplete="off" placeholder="Enter Name" name="user_name">
         </div>
				 <div class="col-md-3">
           <input type="text" class="form-control mb-2" value="{{request('mobile')}}" autocomplete="off" placeholder="Enter Phone" name="mobile">
         </div>
				 <div class="col-md-3">
           <input type="text" class="form-control mb-2" value="{{request('email')}}" autocomplete="off" placeholder="Enter Email" name="email">
         </div>
         <div class="col-md-3">
           <button class="btn btn-info mb-2">Search</button>
         </div>
       </form>
     </div>
     
    <div class="table-responsive ">
        <!--Table-->
        <table class="table table-striped table-bordered" style="width: 88%; overflow: auto">

          <thead style="background: #0066ff; color: #fff">
  
    <tr>
<td> <input type="checkbox" id="master"> </td>
      <th scope="col">S.no.</th>
      <th scope="col">User Name</th>
      <th scope="col">Phone</th>
      <th scope="col">Email</th>
      <th scope="col">Joining Date</th>
      {{-- <td scope="col">Edit</td> --}}
       <td scope="col">Delete</td>
      

     </tr>
  </thead>
  <?php $i=1; ?>
  @forelse($users as $user)
  <tbody>
  	
  	<tr scope="col" style="background: #e6f2ff;"> 
			<td><input type="checkbox" class="sub_chk" data-id="{{$user->id}}"></td>

  		<td>{{ $i++ }}</td>
     
      <td>{{$user->name}} </td>

      
      <td>{{$user->mobile}}</td>
      <td>{{$user->email}}</td>
      <td>{{$user->created_at}}</td>
     {{-- <td>
      <button type="button" class="btn btn-primary" href="/admin/edit-user-detail/{{$user->id}}">Edit</button>

      <button type="button" data-myid="{{$user->id}}" data-myname="{!!$user->name!!}" data-mymodel="{{$user->brand_model_name}}" data-myemail="{{$user->email}}" data-mymobile="{{$user->mobile}}"  class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo">Edit</button></td> --}}
      {{-- <td><button type="button" href="/admin/user-lists/{{$user->id}}" class="btn btn-danger">Delete</a></td> --}}
      <td> <button type="button" data-id="{{$user->id}}" class="btn btn-danger" data-toggle="modal"
          data-target="#deleteModal" data-whatever="@mdo">Delete</button> </td>
          <a href="#" class="hidden" hidden data-tr="tr_{{$user->id}}"
                           data-toggle-delete="confirmation"
                           data-btn-ok-label="Delete" data-btn-ok-icon="fa fa-remove"
                           data-btn-ok-class="btn btn-sm btn-danger"
                           data-btn-cancel-label="Cancel"
                           data-btn-cancel-icon="fa fa-chevron-circle-left"
                           data-btn-cancel-class="btn btn-sm btn-default"
                           data-title="Are you sure you want to delete ?"
                           data-placement="left" data-singleton="true"></a>

	</tr>
  	
  </tbody>
 @empty
    <p style="color: red;">No  Details</p>
  @endforelse 
</table>
	</div>
	</div>



<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Update</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="/admin/brand_name-edit" method="post" enctype="multipart/form-data">
          @csrf
          <div class="form-group">
            
            <input type="hidden" name="id" class="form-control" id="id">
          </div>
          <div class="form-group">
            
            <input type="text" name="brand_name" class="form-control" id="name">
          </div>
          <div class="form-group">
            
            <input type="text" name="brand_model_name" class="form-control" id="Model">
          </div>
           <div class="form-group">
           
            <input type="file" name="images" class="form-control" id="email">
          </div>
         
          
          <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit"  class="btn btn-primary">Update</button>
      </div>
        </form>
      </div>
     
    </div>
  </div>
</div>   


<!-- delete model -->
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="deleteModalLabel">Delete</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="/admin/user-delete" method="post" enctype="multipart/form-data">
          @csrf
          <div class="form-group">
            <input type="text" hidden name="id" class="form-control hidden" id="id">
          </div>
					<div class="form-group">
						<h3>Are you sure want to delete this</h3>
					</div>
          <div class="modal-footer">
		        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
		        <button type="submit"  class="btn btn-primary">Delete</button>
		      </div>
        </form>
      </div>
    </div>
  </div>
</div>


<script type="text/javascript">
$('#exampleModal').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget) // Button that triggered the modal
  var recipient = button.data('myid')
  var name = button.data('mybrand')
  var email = button.data('myemail') 
  var model = button.data('mymodel') 
  var about = button.data('myabout') 

  var modal = $(this)
 
  modal.find('.modal-body #id').val(recipient)
   modal.find('.modal-body #email').val(email)
   modal.find('.modal-body #name').val(name)
   modal.find('.modal-body #Model').val(model)
   modal.find('.modal-body #about').val(about)

})


// delete confirm model method on open model
$('#deleteModal').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget) // Button that triggered the modal
  var id = button.data('id')
  var modal = $(this)
  modal.find('.modal-body #id').val(id)
})

// delet selected


        $('#master').on('click', function(e) {
         if($(this).is(':checked',true))
         {
            $(".sub_chk").prop('checked', true);
         } else {
            $(".sub_chk").prop('checked',false);
         }
        });


        $('.delete_all').on('click', function(e) {

            var allVals = [];
            $(".sub_chk:checked").each(function() {
                allVals.push($(this).attr('data-id'));
            });


            if(allVals.length <=0)
            {
                alert("Please select row.");
            }  else {


                var check = confirm("Are you sure you want to delete this row?");
                if(check == true){

                    var join_selected_values = allVals.join(",");


                    $.ajax({
                        url: $(this).data('url'),
                        type: 'DELETE',
                        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                        data: 'ids='+join_selected_values,
                        success: function (data) {
                            if (data['success']) {
                                $(".sub_chk:checked").each(function() {
                                    $(this).parents("tr").remove();
                                });
                                alert(data['success']);
                            } else if (data['error']) {
                                alert(data['error']);
                            } else {
                                alert('Whoops Something went wrong!!');
                            }
                        },
                        error: function (data) {
                            alert(data.responseText);
                        }
                    });


                  $.each(allVals, function( index, value ) {
                      $('table tr').filter("[data-row-id='" + value + "']").remove();
                  });
                }
            }
        });





        $(document).on('confirm', function (e) {
            var ele = e.target;
            e.preventDefault();


            $.ajax({
                url: ele.href,
                type: 'DELETE',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success: function (data) {
                    if (data['success']) {
                        $("#" + data['tr']).slideUp("slow");
                        alert(data['success']);
                    } else if (data['error']) {
                        alert(data['error']);
                    } else {
                        alert('Whoops Something went wrong!!');
                    }
                },
                error: function (data) {
                    alert(data.responseText);
                }
            });


            return false;
        });
    </script>
   {!! $users->links() !!}
    
@endsection