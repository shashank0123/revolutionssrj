<?php
use App\brandDetails;
// use App\Service;
?>

@extends('layouts.Dashboard')

@section('content')
<!-- <script src="http://demo.itsolutionstuff.com/plugin/jquery.js"></script>
    <link rel="stylesheet" href="http://demo.itsolutionstuff.com/plugin/bootstrap-3.min.css"> -->
    <!-- jQuery 3 -->
    <script src="{{asset('/bower_components/jquery/dist/jquery.min.js')}}"></script>
    <!-- Bootstrap 3.3.7 -->
    <script src="{{asset('/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>

    <!-- Latest compiled and minified CSS -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css" />
  <!-- Latest compiled and minified JavaScript -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>
<style media="screen">
.d-none {
  display: none !important
}
</style>

 <div class="container-fluid" style="background: #fff" >
 	<div class="row" style="border:5px solid black;"style="background: #fff" >
 	<div class="col-sm-6" >
 	<h3 style="text-align: center;"><u><b>Upload Homepage Slider</b></u></h3>
 	 	<form action="/admin/slider" method="post" enctype="multipart/form-data">
 	 		@csrf
  <div class="form-group">
    <label for="exampleFormControlSelect1">Upload Slider Image</label>
    <input type="file" name="images" class="form-control" id="exampleFormControlInput1" placeholder="Upload Slider Image" required="">
    <h6>&nbsp;&nbsp;Image size should be 1400*510</h6>
  </div>

  <div>
  	<button type="submit" class="btn btn-success"> Submit</button>
  </div>
 <br>
</form>
 </div>
 <div class="col-sm-6"  >
  <div style="padding-top: 80px;">
   <a href="/admin/slider-image" class="btn btn-info"  style="width:30%; ">View</a></div>
 </div>
</div>
<br>
<div class="row" style="border:5px solid black;" >
  <!-- add brand name  -->
	<div class="col-sm-4">
		<h3 style="text-align: center;"><u><b>Add Brand Name</b></u></h3>

 	<form action="/admin/brandname" method="post" enctype="multipart/form-data">
 	 		@csrf
  <div class="form-group">
    <label for="exampleFormControlSelect1">Brand Name</label>
    <input type="text" class="form-control" name="BrandName" id="exampleFormControlInput1" placeholder="Type Mobile Brand Name" required="">
  </div>
  <div class="form-group">
    <label for="exampleFormControlSelect1">Brand Logo </label>
    <input type="file" class="form-control" name="BrandLogo" id="exampleFormControlInput1" placeholder="Type Mobile Brand Name" required="">
  </div>
  <div class="form-group">
    <label for="brand_title">Brand Title</label>
    <input type="text" class="form-control" name="brand_title" id="brand_title" placeholder="Type Brand Title" required>
  </div>
  <div class="form-group">
    <label>Brand Discription (*optional)</label>
    <textarea class="form-control textarea" name="brand_disc" rows="3" placeholder="Write something about brand" ></textarea>
  </div>

<button class="btn btn-success" type="submit">Submit</button>
<a href="/admin/showbrandDetails" class="btn btn-info" >View</a>


	</form>

</div>

  <!-- brand available status -->
  <div class="col-sm-8">
    <h3 style="text-align: center;"><u><b>Add Brand Model</b></u></h3>

  <form action="/admin/brandmodel" method="post" enctype="multipart/form-data">
      @csrf
  <div class="form-group">
    <label for="exampleFormControlSelect1">Brand Name</label>
    <select name="brand_id" class="form-control">
       <?php
        $data=brandDetails::all();
        foreach ($data as $key => $value) {
        ?>
        <option value="{{$value->id}}">{{$value->brand_name}}</option>
        <?php }?>
    </select>
  </div>
  <div class="form-group">
    <label for="exampleFormControlSelect1">Brand Model</label>
    <input type="text" class="form-control" name="BrandModel"  placeholder="Type Mobile Model Name" required="">
  </div>
   <div class="form-group">
    <label for="exampleFormControlSelect1">Brand Model Image</label>
    <input type="file" class="form-control" name="BrandModelImage"  placeholder="choose brand model image" required="">

  </div>

  <div class="form-group d-none">
    <label for="exampleFormControlSelect1">Market Price</label>
    <input type="text" class="form-control" name="actualprice"  value="0"  placeholder="actualprice" required="">

  </div>

  <!-- <div class="form-group">-->
  <!--  <label for="exampleFormControlSelect1">Selling Price</label>-->
  <!--  <input type="text" class="form-control" name="SellingPrice"  placeholder="Selling Price" required="">-->

  <!--</div>-->

  <div class="form-group d-none">
    <label for="exampleFormControlSelect1">Spazeme Price</label>
    <input type="text" class="form-control" name="ScreenproPrice"  value="0"  placeholder="Spazeme Price" required="">

  </div>
  <div class="form-group">
    <label for="model_title">Model Title</label>
    <input type="text" class="form-control" name="model_title" id="model_title" placeholder="Type Model Title" required>
  </div>

  <div class="form-group">
    <label>Text About Model(*optional)</label>
     <textarea class="textarea" name="about" rows="3" placeholder="Write something about model"
                             ></textarea>

  </div>
  <div>
<button class="btn btn-success" type="submit">Submit</button>
<a href="/admin/showModel" class="btn btn-info" >View</a>

</div>

<br>
</form>

</div>
</div>

<hr>
 <div class="col-sm-12" style="border:5px solid black;">
 	<div class="col-sm-6">
    <h3 style="text-align: center;"><u><b>Available State </b></u></h3>

  <form action="/admin/AvailableStatus" method="post" enctype="multipart/form-data">
      @csrf

  <div class="form-group">
      <label for="state">Available States</label>
    <input type="text" name="state" id="state" class="form-control" placeholder="Enter available location" required>

  </div>
  <!--<div class="form-group">-->
  <!--  <label>Pincode</label>-->
  <!--  <input type="number" name="pincode" class="form-control" placeholder="Enter pin for available states" required="">-->
  <!--</div>-->
  <div class="form-group">
    <label>Image</label>
    <input type="file" name="image" class="form-control" required="">
  </div>
  <div class="form-group">
    <label>Status</label>
    <select name="status" class="form-control" required="" >
      <option>Available</option>
      <option>Not Available</option>

    </select>
  </div>

  <div>
    <button type="submit" class="btn btn-success"> Submit</button>
<a href="/admin/show/AvailableStatus" class="btn btn-info" >View</a>

  </div>
</form>
 <br>
 </div>

  <div class="col-sm-6" style="border-left: 2px solid black; height:405px
  ">
    <h3 style="text-align: center;"><u><b>Add Brand color</b></u></h3>

  <form action="/admin/brandcolor" method="post" enctype="multipart/form-data">
      @csrf
  <div class="form-group">
    <label for="brand_name_of_color">Brand Name</label>
    <select id="brand_name_of_color" name="brand_name" class="form-control">
       <option>select</option>
       <?php
      $data=brandDetails::all();
      foreach ($data as $key => $value) {
      ?>
      <option value="{{$value->id}}">{{$value->brand_name}}</option>
      <?php }?>
    </select>
  </div>
  <div class="form-group">
    <label for="exampleFormControlSelect1">Brand Model </label>
   <select name="brand_model" required="" class="form-control">
    <!-- <option>select</option> -->
   </select>

  </div>
   <div class="form-group">
    <label for="exampleFormControlSelect1">Brand Color</label>
    <input type="text" class="form-control" name="color"  placeholder="Brand color" required="">

  </div>

  <div>
<button class="btn btn-success" type="submit">Submit</button>
<a href="/admin/brandcolor-page" class="btn btn-info">View</a>

</div>
</form>
  </div>

</div>


<script src="https://cdn.ckeditor.com/4.11.4/standard/ckeditor.js"></script>
  <script>

            CKEDITOR.replace( 'about' );
            CKEDITOR.replace( 'about' );

</script>

<script type="text/javascript">
    $(document).ready(function() {
        $('select[id="brand_name_of_color"]').on('change', function() {
            var stateID = $(this).val();
            if(stateID) {
                $.ajax({
                    url: '/admin/myform/ajax/'+stateID,
                    type: "GET",
                    dataType: "json",
                    success:function(data) {


                        $('select[name="brand_model"]').empty();
                        $.each(data, function(key, value) {
                            $('select[name="brand_model"]').append('<option value="'+ value.id+'">'+ value.brand_model_name +'</option>');
                        });

                    }
                });
            }else{
                $('select[name="brand_model"]').empty();
            }
        });
    });
</script>

@endsection
