<?php
use App\brandDetails;
// use App\Service;
?>

@extends('layouts.Dashboard')

@section('content')

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
	<div class="container">
     <div style="text-align: center;" ><h3><u><b>Edit Brand Model</b></u></h3></div>
     <div class="row mb-rem-1 search-content-row" >
       <form>
         <!--<div class="col-md-2">-->
         <!--  <input type="text" class="form-control mb-2" value="{{request('brand_name')}}" autocomplete="off" placeholder="Enter Brand Name" name="brand_name" style="font-size: 14px;">-->
         <!--</div>-->
				 <div class="col-md-4">
           <input type="text" class="form-control mb-2" value="{{request('brand_model_name')}}" autocomplete="off" placeholder="Enter Model Name" name="brand_model_name" style="font-size: 14px;">
         </div>
 				<div class="col-md-2">
           <select name="service_status" class="form-control" id="service_status" value="{{request('service_status')}}">
             <option value="all" @if(request('service_status') == 'all') selected @endif>All</option>
             <option value="active" @if(request('service_status') == 'active') selected @endif >Active</option>
             <option value="inactive" @if(request('service_status') == 'inactive') selected @endif >Inactive</option>
           </select>
         </div>
         <div class="col-md-2">
           <select name="order_by_column" class="form-control" id="order_by_column" value="{{request('order_by_column')}}">
             <option value="all" @if(request('order_by_column') == 'all') selected @endif >Default</option>
 						<option value="created_at" @if(request('order_by_column') == 'created_at')@endif >Created At</option>
             <option value="service_name" @if(request('order_by_column') == 'service_name') selected @endif>Service Name</option>
           </select>
         </div>
 				<div class="col-md-2">
           <select name="order_by_value" class="form-control" id="order_by_value" value="{{request('order_by_value')}}">
             <option value="all" @if(request('order_by_value') == 'all') selected @endif >Default</option>
 						<option value="desc" @if(request('order_by_value') == 'desc') selected @endif >Descending Order</option>
             <option value="asc" @if(request('order_by_value') == 'asc') selected @endif >Ascending Order</option>
           </select>
         </div>
         <div class="col-md-2">
           <button class="btn btn-info mb-2">Search</button>
         </div>
       </form>
     </div>
    <div class="table-responsive ">
        <!--Table-->
        <table class="table table-striped table-bordered" style="width: 88%; overflow: auto">

          <thead style="background: #0066ff; color: #fff">

    <tr>

      <th scope="col">S.no.</th>
      <th scope="col">Brand Name</th>
      <th scope="col">Brand Model Name</th>
			<!--<th scope="col">Service Name </th>-->
      <th scope="col">Image</th>
      <!--<th scope="col">Market Price</th>-->
      <!--<th scope="col">Selling Price</th>-->
      <!--<th scope="col">Spazeme Price</th>-->
      <th scope="col">About</th>
      <td scope="col">Edit</td>
      <td scope="col">Delete</td>

     </tr>
  </thead>
  @forelse($show as $row)
  <tbody>

  	<tr scope="col" style="background: #e6f2ff;">

  		<td>{{ $loop->iteration }}</td>
      <?php
             $user_id=$row['brand_id'];

      $data=brandDetails::all()->where('id','=',$user_id) ?>
      <td><?php foreach ($data as $key => $value) {
        echo $value->brand_name;
      } ?></td>
      <td> {{$row->brand_model_name}} </td>
			
      <td><img src="images/{{$row->brand_image}}" width="60px" height="60px"></td>
      <!--<td>{{$row->actualprice}}</td>-->
      <!--<td>{{$row->sellingprice}}</td>-->
      <!--<td>{{$row->screenproPrice}}</td>-->
      <td>{!!str_limit($row->about)!!}</td>
      <td><a href="/admin/brand_edit-model/{{$row->id}}" class="btn btn-info">edit</a></td>
  		</td>
      <td><a href="/admin/brand_model/{{$row->id}}" class="btn btn-danger">Delete</a></td>

	</tr>

  </tbody>
 @empty
    <p style="color: red;">No  Details</p>
  @endforelse
</table>
	</div>
	</div>

{!! $show->links() !!}

    <script src="https://cdn.ckeditor.com/4.11.4/standard/ckeditor.js"></script>
  <script>

            CKEDITOR.replace( 'about' );
            CKEDITOR.replace( 'about' );

</script>

@endsection
