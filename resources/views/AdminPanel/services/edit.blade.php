@extends('layouts.Dashboard')

@section('content')
<div class="container-fluid" style="border:5px solid black;"style="background: #fff">
  <div class="row" >
   	<div class="col-sm-6" >
   	<h3 style="text-align: center;"><u><b>Edit Service</b></u></h3>

   </div>
  </div>
  <form method="POST" action="{{ route('services.update', $service->id) }}"  enctype="multipart/form-data">
      @csrf
      @method('PUT')
      <div class="row mb-2">
        <div class="form-group col-md-3">
            <label for="service_name" class="col-form-label text-md-right">{{ __('Name') }}</label>

            <input id="service_name" type="text" class="form-control @error('service_name') is-invalid @enderror" name="service_name" value="{{ $service->service_name }}" required autocomplete="service_name" autofocus>

            @error('service_name')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>

        <div class="form-group col-md-3">
            <label for="service_image" class="col-form-label text-md-right">{{ __('Image') }}</label>

            <input id="service_image" type="file" class="form-control @error('service_image') is-invalid @enderror" name="service_image" value="{{ old('service_image') }}" required autocomplete="service_image" autofocus>

            @error('service_image')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>

        <div class="form-group col-md-3">
            <img src="{{asset($service->ImageUrl)}}" class="img-responsive" width="50" height="50" alt="service image ">
        </div>

          <div class="form-group col-md-3">
              <label for="status" class=" col-form-label text-md-right">{{ __('Status') }}</label>
              <select id="status" type="text" class="form-control @error('status') is-invalid @enderror" name="status" value="{{ $service->status }}" required autocomplete="status" >
                <option value="active" >Active</option>
                <option value="inactive">Inactive</option>
              </select>
              @error('status')
                  <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                  </span>
              @enderror
          </div>
      </div>
      <div class="form-group row mb-0">
          <div class="col-md-6 offset-md-4">
              <button type="submit" class="btn btn-primary">
                  {{ __('Update') }}
              </button>
          </div>
      </div>
  </form>
</div>
@endsection
