@extends('layouts.Dashboard')

@section('content')

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>

	<div class="container">
    <div class="row">
      <div class="col-md-5">
        <a href="services/create" class="btn btn-info">Create</a>
      </div>
      <div class="col-md-7">
        <h4>Services</h4>
      </div>
    </div>

    <div class="row mb-rem-1 search-content-row" >
      <form>
        <div class="col-md-3">
          <input type="text" class="form-control mb-2" value="{{request('service_name')}}" autocomplete="off" placeholder="Enter Service Name" name="service_name" style="font-size: 14px;">
        </div>
				<div class="col-md-2">
          <select name="service_status" class="form-control" id="service_status" value="{{request('service_status')}}">
            <option value="all" @if(request('service_status') == 'all') selected @endif>All</option>
            <option value="active" @if(request('service_status') == 'active') selected @endif >Active</option>
            <option value="inactive" @if(request('service_status') == 'inactive') selected @endif >Inactive</option>
          </select>
        </div>
        <div class="col-md-2">
          <select name="order_by_column" class="form-control" id="order_by_column" value="{{request('order_by_column')}}">
            <option value="all" @if(request('order_by_column') == 'all') selected @endif >Default</option>
						<option value="created_at" @if(request('order_by_column') == 'created_at')@endif >Created At</option>
            <option value="service_name" @if(request('order_by_column') == 'service_name') selected @endif  >Service Name</option>
          </select>
        </div>
				<div class="col-md-3">
          <select name="order_by_value" class="form-control" id="order_by_value" value="{{request('order_by_value')}}">
            <option value="all" @if(request('order_by_value') == 'all') selected @endif >Default</option>
						<option value="desc" @if(request('order_by_value') == 'desc') selected @endif >Descending Order</option>
            <option value="asc" @if(request('order_by_value') == 'asc') selected @endif >Ascending Order</option>
          </select>
        </div>
        <div class="col-md-2">
          <button class="btn btn-info mb-2">Search</button>
        </div>
      </form>
    </div>

    <div class="table-responsive text-capitalize">
      <!--Table-->
      <table class="table table-striped table-bordered" style="width: 88%; overflow: auto">

        <thead style="background: #0066ff; color: #fff">
          <tr>
            <th scope="col">S.no.</th>
            <th scope="col">Service Name</th>
            <th scope="col">Image</th>
            <th scope="col">Status</th>
            <th scope="col">Created At Date</th>
            <th scope="col" class="text-center" colspan="3">Action</th>
            <!-- <th scope="col">Delete</th> -->
           </tr>
        </thead>
        @if($services)
        @forelse($services as $row)
        <tbody>
          <tr scope="col" style="background: #e6f2ff;">
            <td>{{ $loop->iteration }}</td>
            <td>{{$row->service_name}}</td>
            <td><img src="{{$row->ImageUrl}}" class="img-responsive" width="50" height="50" alt="service image "></td>
            <td> {{$row->status}} </td>
            <td>{!!$row->created_at!!}</td>
            <td><a href="/admin/services/{{$row->id}}/edit" class="btn btn-info">edit</a></td></td>
						<td><a href="/admin/model-services?serviceId={{$row->id}}" class="btn btn-info">Manage Model</a></td></td>
            <!-- <td><a href="services/delete" class="btn btn-danger">Delete</a></td> -->
						<td>
							<button type="button" data-myid="{{$row->id}}" class="btn btn-danger" data-toggle="modal"
				          data-target="#deleteModal" data-whatever="@mdo">Delete</button>
						</td>
          </tr>
        </tbody>
       @empty
       <tbody>
         <tr scope="col" style="background: #e6f2ff;">
           <td colspan="6" class="text-center">
              <p style="color: red;">No Data Found</p></td>
         </tr>
       </tbody>
        @endforelse
        @endif
      </table>
	   </div>
	</div>
{!! $services->links() !!}

<!-- START delete model -->
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="deleteModalLabel">Delete</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="services-delete" method="POST">
					@method('DELETE')
          @csrf
          <div class="form-group">
            <input type="text" name="id" class="form-control hidden" id="id">
          </div>
					<div class="form-group">
						<h3>Are you sure want to delete this</h3>
					</div>
          <div class="modal-footer">
		        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
		        <button type="submit"  class="btn btn-primary">Delete</button>
		      </div>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- END delete model -->

<script type="text/javascript">
// delete confirm model method on open model
$('#deleteModal').on('show.bs.modal', function (event) {
	var button = $(event.relatedTarget) // Button that triggered the modal
	var id = button.data('myid')
	var modal = $(this)
	modal.find('.modal-body #id').val(id)
})
</script>

@endsection
