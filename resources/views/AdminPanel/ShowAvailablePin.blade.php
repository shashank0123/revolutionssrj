@extends('layouts.Dashboard')

@section('content')

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
	<div class="container">
     <div style="text-align: center;" ><h3><u><b>Available States  </b></u></h3></div>
     <div class="form-group">
                
             </div>
    <div class="table-responsive ">
        <!--Table-->
        <table class="table table-striped table-bordered" style="width: 88%;overflow: auto">

          <thead style="background: #0066ff; color: #fff">
  
    <tr>

      <th scope="col">S.no.</th>
      <th scope="col">State</th>
      <th scope="col">Pin</th>
      <th scope="col">Status</th>

      <td scope="col">Active/Inactive</td>
      <!-- <td scope="col">Delete</td> -->

     </tr>
  </thead>
  @forelse($show as $row)
  <tbody>
  	
  	<tr scope="col" style="background: #e6f2ff;"> 

  		<td>{{ $loop->iteration }}</td>
      <td>{{$row->state}}</td>
      <td>{{ $row->pin }}</td>
      <td>{{$row->status}}</td>
      <td>
				<button type="button" class="btn btn-primary" data-myid="{{$row->id}}" data-mystate="{{$row->state}}"
					data-status="{{$row->status}}" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo">Edit</button>
			</td>
      <!--<td><a href="" class="btn btn-danger">Activate</a></td>-->
  		
     <!--  <td><a href="/admin/slider-image/delete/{{$row->id}}" class="btn btn-danger">Delete</a></td> -->

	</tr>
  	
  </tbody>
 @empty
    <p style="color: red;">No states</p>
  @endforelse 
</table>
	</div>
	</div>
	
	
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="exampleModalLabel">Update</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
	        <form action="/admin/state-available-edit" method="post" enctype="multipart/form-data">
	          @csrf
	          <div class="form-group">

	            <input type="text" name="id" class="form-control hidden" id="id">
	          </div>
	          <div class="form-group">
					    <label for="state">State</label>
	            <input type="text" id="state" name="state" disabled class="form-control">
	          </div>

					  <div class="form-group">
					    <label for="status">Status</label>
					    <select name="status" id="status" class="form-control" required >
					      <option>Available</option>
					      <option>Not Available</option>
					    </select>
					  </div>

	          <div class="modal-footer">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	        <button type="submit"  class="btn btn-primary">Update</button>
	      </div>
	        </form>
	      </div>

	    </div>
	  </div>
	</div>

	<script type="text/javascript">
	$('#exampleModal').on('show.bs.modal', function (event) {
	  var button = $(event.relatedTarget) // Button that triggered the modal
	  var id = button.data('myid')
	  var state = button.data('mystate')
	  var status = button.data('status') // Extract info from data-* attributes
	  var modal = $(this)

	  modal.find('.modal-body #id').val(id)
	   modal.find('.modal-body #state').val(state)
	   modal.find('.modal-body #status').val(status)

	})


	    </script>



@endsection