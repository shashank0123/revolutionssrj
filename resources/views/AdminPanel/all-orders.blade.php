<?php
use App\brandDetails;
use App\brandModel;

?>

@extends('layouts.Dashboard')

@section('content')
<meta name="csrf-token" value="{{csrf_token()}}">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>

<div class="container-fluid">
 <div style="text-align: center;" ><h3><u><b>Orders List</b></u></h3></div>
   <div class="row" style="
    margin-bottom: 1rem;
">
     <form >
       <div class="col-md-3">
         <input type="text" class="form-control mb-2" value="{{ request('query_name') }}" autocomplete="off" placeholder="Type username..." name="query_name" style="font-size: 14px;">
       </div>
       <div class="col-md-3">
         <input type="text" class="form-control mb-2" value="{{ request('query_mobile') }}" autocomplete="off" placeholder="Type mobile..." name="query_mobile" style="font-size: 14px;">
       </div>
       <div class="col-md-3">
         <select name="query_order_status" class="form-control" id="query_order_status" {{ request('query_order_status') }}>
           <option value="all" >All</option>
           <option value="Order_cancelled" >Cancelled</option>
           <option value="Confirm_Order" >Confirmed</option>
           <option value="agent_assign" >Agent Assigned</option>
           <option value="delivery" >Delivery</option>
         </select>
       </div>
       <div class="col-md-3">
         <button class="btn btn-info mb-2">Search</button>
       </div>
     </form>
   </div>
<div class="row my-2">
  <div class="table-responsive ">
   <!--Table-->
   <table class="table table-striped table-bordered">

     <thead style="background: #0066ff; color: #fff">

       <tr class="text-nowrap">

         <th scope="col">S.no.</th>
         <th scope="col">Customer Name</th>
         <th scope="col">Email </th>
         <th scope="col">Mobile</th>
         {{--<th scope="col">Brand Name</th>--}}
         <th scope="col">Brand Model</th>
         <th scope="col">Brand color</th>
         <th scope="col">Price</th>
         <th scope="col">Status</th>
         <th scope="col">Service Date</th>
         <th scope="col">Change Status</th>
         <th scope="col">Invoice</th>
         <th scope="col">Delete</th>

       </tr>
     </thead>
     <?php $i=1; ?>
     @forelse($orders as $order)
     <tbody>

      <tr scope="col" style="background: #e6f2ff;">

       <td>{{ $i++ }}</td>
       <td>{{$order->username}}</td>
       <td>{{ str_limit($order->email, 20) }}</td>
       <td>{{$order->phone}}</td>
       {{--<td>{{$order->Brand_Name}}</td>--}}
       <td>{{$order->Brand_Model}}</td>
       <td>{{$order->Brand_color}}</td>
       <td>{{$order->price}}</td>
       <td>{{str_replace('_', ' ', $order->track_point)}}</td>
       <td>{{$order->service_time}}</td>
       <td>
         <form method="POST">
           {{-- {{csrf --}}
           {{-- {{ csrf_token() }} --}}
         <select name="track_point" class="form-control" id="order_status_{{$order->id}}" onchange="changeStatus({{$order->id}})">
             <option value="" class="d-none" style="display: none;"> Select Status</option>
           <option value="Order_cancelled" <?php if($order->track_point=="Order_cancelled") {echo 'selected';} ?> >Cancelled</option>
           <option value="Confirm_Order" <?php if($order->track_point=="Confirm_Order") {echo 'selected';} ?> >Confirmed</option>
           <option value="agent_assign" <?php if($order->track_point=="agent_assign") {echo 'selected';} ?> >Agent Assigned</option>
           <option value="delivery" <?php if($order->track_point=="delivery") {echo 'selected';} ?> >Delivery</option>

         </select>

       </form>
         </td>
           {{-- <td>{{$order->service_time}}</td> --}}
         <td><a href="/admin/invoice/{{$order->id}}"><input type="submit" class="btn btn-primary" value="Invoice"></a></td>

         {{-- <td> --}}
           {{-- <button type="button" class="btn btn-primary" href="/admin/edit-order/{{$order->id}}">Edit</button> --}}

           {{-- <button type="button" data-myid="{{$order->id}}" data-myname="{!!$order->name!!}" data-mymodel="{{$order->brand_model_name}}" data-myemail="{{$order->email}}" data-mymobile="{{$order->mobile}}"  class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo">Edit</button></td> --}}
       {{-- <td><a href="/admin/user-lists/{{$user->id}}" class="btn btn-danger">Delete</a></td>
       --}}
       <td> <button type="button" data-id="{{$order->id}}" class="btn btn-danger" data-toggle="modal"
         data-target="#deleteModal" data-whatever="@mdo">Delete</button> </td>
     </tr>

   </tbody>
   @empty
   <p style="color: red;">No  Details</p>
   @endforelse
 </table>
 </div>
</div>

</div>



<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Update</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="/admin/brand_name-edit" method="post" enctype="multipart/form-data">
          @csrf
          <div class="form-group">

            <input type="hidden" name="id" class="form-control" id="id">
          </div>
          <div class="form-group">

            <input type="text" name="brand_name" class="form-control" id="name">
          </div>
          <div class="form-group">

            <input type="text" name="brand_model_name" class="form-control" id="Model">
          </div>
          <div class="form-group">

            <input type="file" name="images" class="form-control" id="email">
          </div>


          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit"  class="btn btn-primary">Update</button>
          </div>
        </form>
      </div>

    </div>
  </div>
</div>


<!-- delete model -->
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="deleteModalLabel">Delete</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="/admin/order-delete" method="post" enctype="multipart/form-data">
          @csrf
          <div class="form-group">
            <input type="text" hidden name="id" class="form-control hidden" id="id">
          </div>
					<div class="form-group">
						<h3>Are you sure want to delete this</h3>
					</div>
          <div class="modal-footer">
		        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
		        <button type="submit"  class="btn btn-primary">Delete</button>
		      </div>
        </form>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  $('#exampleModal').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget) // Button that triggered the modal
  var recipient = button.data('myid')
  var name = button.data('mybrand')
  var email = button.data('myemail')
  var model = button.data('mymodel')
  var about = button.data('myabout')

  var modal = $(this)

  modal.find('.modal-body #id').val(recipient)
  modal.find('.modal-body #email').val(email)
  modal.find('.modal-body #name').val(name)
  modal.find('.modal-body #Model').val(model)
  modal.find('.modal-body #about').val(about)

});

function changeStatus(id){
  var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
  var orderID = id;
  var status = $('#order_status_'+id).val();
//   alert(id+" "+status);

  $.ajax({
    url:"/admin/change-status",
    type: 'POST',
    data: {_token: CSRF_TOKEN, ID: orderID, status: status},

    success:function(data){

        window.location.href = "/admin/orders";

    }
  });
}

// delete confirm model method on open model
$('#deleteModal').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget) // Button that triggered the modal
  var id = button.data('id')
  var modal = $(this)
  modal.find('.modal-body #id').val(id)

})
</script>
{!! $orders->links() !!}

@endsection
