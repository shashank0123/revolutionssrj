<?php
use App\brandDetails;
use App\brandModel;
?>

@extends('layouts.Dashboard')

@section('content')

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
	<div class="container">
     <div style="text-align: center;" ><h3><u><b>Edit Brand color</b></u></h3></div>
     <div class="row mb-rem-1 search-content-row" >
			 <form>
				 <!-- <div class="col-md-2">
					 <input type="text" class="form-control mb-2" value="{{request('brand_name')}}" autocomplete="off" placeholder="Enter Brand Name" name="brand_name" style="font-size: 14px;">
				 </div> -->
				 <!-- <div class="col-md-2">
					 <input type="text" class="form-control mb-2" value="{{request('brand_model_name')}}" autocomplete="off" placeholder="Enter Model Name" name="brand_model_name" style="font-size: 14px;">
				 </div> -->
				 <div class="col-md-4">
					 <input type="text" class="form-control mb-2" value="{{request('color_name')}}" autocomplete="off" placeholder="Enter Color Name" name="color_name" style="font-size: 14px;">
				 </div>
				<div class="col-md-2">
					 <select name="service_status" class="form-control" id="service_status" value="{{request('service_status')}}">
						 <option value="all" @if(request('service_status') == 'all') selected @endif>All</option>
						 <option value="active" @if(request('service_status') == 'active') selected @endif >Active</option>
						 <option value="inactive" @if(request('service_status') == 'inactive') selected @endif >Inactive</option>
					 </select>
				 </div>
				 <div class="col-md-2">
					 <select name="order_by_column" class="form-control" id="order_by_column" value="{{request('order_by_column')}}">
						 <option value="all" @if(request('order_by_column') == 'all') selected @endif >Default</option>
						<option value="created_at" @if(request('order_by_column') == 'created_at')@endif >Created At</option>
						 <option value="service_name" @if(request('order_by_column') == 'service_name') selected @endif>Service Name</option>
					 </select>
				 </div>
				<div class="col-md-2">
					 <select name="order_by_value" class="form-control" id="order_by_value" value="{{request('order_by_value')}}">
						 <option value="all" @if(request('order_by_value') == 'all') selected @endif >Default</option>
						<option value="desc" @if(request('order_by_value') == 'desc') selected @endif >Descending Order</option>
						 <option value="asc" @if(request('order_by_value') == 'asc') selected @endif >Ascending Order</option>
					 </select>
				 </div>
				 <div class="col-md-2">
					 <button class="btn btn-info mb-2">Search</button>
				 </div>
			 </form>
		 </div>
    <div class="table-responsive ">
        <!--Table-->
        <table class="table table-striped table-bordered">

          <thead style="background: #0066ff; color: #fff">

    <tr>

      <th scope="col">S.no.</th>
      <th scope="col">Brand Name</th>
      <th scope="col">Brand Model Name</th>
      <th scope="col">color</th>
      <td scope="col">Edit</td>
      <td scope="col">Delete</td>

     </tr>
  </thead>
  @forelse($show as $row)
  <tbody>

  	<tr scope="col" style="background: #e6f2ff;">

  		<td>{{ $loop->iteration }}</td>
      <?php
             $user_id=$row['brand_id']; ?>
      <td>
				{{$row['brandModelName']}}
		</td>

      <?php

      $model_id=$row['brand_model_id'];?>
      <td>
				{{$row['brandName']}}
			</td>
      <td>{{$row->color}}</td>
     <td>
			 <button type="button" data-id="{{$row->id}}"
				 data-color="{{$row->color}}"
				 class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo">Edit</button>
		 </td>
      <td>
				<button type="button" data-id="{{$row->id}}"
 				 class="btn btn-danger" data-toggle="modal" data-target="#deleteModal" data-whatever="@mdo">Delete</button>
			</td>

	</tr>

  </tbody>
 @empty
    <p style="color: red;">No  Details</p>
  @endforelse
</table>
	</div>
	</div>



<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Update</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="/admin/brandcolor-edit" method="post" enctype="multipart/form-data">
          @csrf
          <div class="form-group">

            <input type="text" hidden name="id" class="form-control hidden" id="id">
          </div>
          <div class="form-group">

            <input type="text" name="color" class="form-control" id="color">
          </div>

          <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit"  class="btn btn-primary">Update</button>
      </div>
        </form>
      </div>

    </div>
  </div>
</div>
<!-- delete model -->
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="deleteModalLabel">Delete</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="/admin/brandcolor-delete" method="post" enctype="multipart/form-data">
          @csrf
          <div class="form-group">
            <input type="text" hidden name="id" class="form-control hidden" id="id">
          </div>
					<div class="form-group">
						<h3>Are you sure want to delete this</h3>
					</div>
          <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
        <button type="submit"  class="btn btn-primary">Delete</button>
      </div>
        </form>
      </div>

    </div>
  </div>
</div>

{!! $show->links() !!}

<script type="text/javascript">
$('#exampleModal').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget) // Button that triggered the modal
  var id = button.data('id')
  var color = button.data('color')

  var modal = $(this)

  modal.find('.modal-body #id').val(id)
  modal.find('.modal-body #color').val(color)

})

$('#deleteModal').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget) // Button that triggered the modal
  var id = button.data('id')
  var modal = $(this)
  modal.find('.modal-body #id').val(id)

})

    </script>


@endsection
