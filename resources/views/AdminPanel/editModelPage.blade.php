@extends('layouts.Dashboard')

@section('content')
<?php
use App\brandDetails;
// use App\Service;
?>
@foreach($show as $row)
    <!-- Latest compiled and minified CSS -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css" />
  <!-- Latest compiled and minified JavaScript -->
  <script src="http://demo.itsolutionstuff.com/plugin/jquery.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>
  <style media="screen">
  .d-none {
    display: none !important
  }
  </style>
<div class="col-sm-12">
<h3 style="text-align: center;"><u><b>Edit Brand Model</b></u></h3>

  <form action="/admin/edit-model-page" method="post" enctype="multipart/form-data">
      @csrf

      <div class="form-group">
    	<input type="hidden" name="id" value="{{$row->id}}">

		</div>

  <div class="form-group">
    <label for="exampleFormControlSelect1">Brand Name</label>
    <select name="brand_id" class="form-control">

     <?php
      $data=brandDetails::all();
      foreach ($data as $key => $value) {


      ?>
      <option value="{{$value->id}}">{{$value->brand_name}}</option>
      <?php }?>
    </select>
  </div>
  <div class="form-group">
    <label for="exampleFormControlSelect1">Brand Model</label>
    <input type="text" class="form-control" value="{{$row->brand_model_name}}" name="BrandModel"  placeholder="Type Mobile Model Name" required="">

  </div>
  <!--<div class="form-group">-->
  <!--  <img src="{{asset('images/'.$row->brand_image)}}" width="60px" height="60px">-->
  <!--</div>-->
  <!-- <div class="form-group">-->
  <!--  <label for="exampleFormControlSelect1">Brand Model Image</label>-->
  <!--  <input type="file" class="form-control" value="{{$row->brand_image}}" name="BrandModelImage"  placeholder="choose brand model image" required="">-->

  <!--</div>-->

  <div class="form-row">
     <div class="col-md-6" style="padding-left: 0px;">
       <div class="form-group">
        <label for="exampleFormControlSelect1">Brand Model Image</label>
        <input type="file" class="form-control" value="{{$row->brand_image}}" name="BrandModelImage"  placeholder="choose brand model image" {{$row->brand_image ? '' : required}} >
      </div>
     </div>
     <div class="col-md-6">
       <div class="form-group">
         <img src="{{asset('images/'.$row->brand_image)}}" width="60px" height="60px">
       </div>
     </div>

   </div>

  <div class="form-group d-none">
    <label for="exampleFormControlSelect1">Market Price</label>
    <input type="text" class="form-control" value="{{$row->actualprice}}" name="actualprice"  placeholder="actualprice" required="">

  </div>

  <!-- <div class="form-group">-->
  <!--  <label for="exampleFormControlSelect1">Selling Price</label>-->
  <!--  <input type="text" class="form-control" name="SellingPrice" value="{{$row->sellingprice}}"  placeholder="Selling Price" required="">-->

  <!--</div>-->

  <div class="form-group d-none">
    <label for="exampleFormControlSelect1">Spazeme Price</label>
    <input type="text" class="form-control" name="ScreenproPrice"  value="{{$row->screenproPrice}}" placeholder="Screenpro Price" required="">

  </div>

   <div class="form-group">
    <label for="model_title">Model Title</label>
    <input type="text" class="form-control" name="model_title" value="{{$row->model_title}}"  id="model_title" placeholder="Type Model Title" required>
  </div>

  <div class="form-group">
    <label>Text About Model(*optional)</label>
     <textarea class="textarea" name="about" rows="3" placeholder="Write something about model"
                             >{!!$row->about!!}</textarea>

  </div>
  <div>
<button class="btn btn-success" type="submit">submit</button>


</div>

<br>
</form>

</div>
</div>
@endforeach
    <script src="https://cdn.ckeditor.com/4.11.4/standard/ckeditor.js"></script>
  <script>

            CKEDITOR.replace( 'about' );
            CKEDITOR.replace( 'about' );

</script>

@endsection
