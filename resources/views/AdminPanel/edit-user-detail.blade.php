@extends('layouts.Dashboard')

@section('content')
<?php
use App\brandDetails;
?>

<div class="col-sm-12">
<h3 style="text-align: center;"><u><b>Edit User Detail</b></u></h3>

  <form action="/admin/edit-user-detail/{{$user->id}}" method="post" enctype="multipart/form-data">
      @csrf

      <div class="form-group">
    	<input type="hidden" name="id" value="{{$user->id}}">
    	
		</div>

  <div class="form-group">
    <label for="name">Name</label>
    <input type="text" class="form-control" value="{{$user->name}}" name="name"  placeholder="Enter User Name" required="">
    {{-- <select name="name" class="form-control">

     <?php
      $data=brandDetails::all();
      foreach ($data as $key => $value) {
        
      
      ?>
      <option value="{{$value->id}}">{{$value->brand_name}}</option>
      <?php }?>
    </select> --}}
  </div>
  <div class="form-group">
    <label for="email">Email</label>
    <input type="email" class="form-control" value="{{$user->email}}" name="email"  placeholder="Enter Email" required="">
    
  </div>
   <div class="form-group">
    <label for="exampleFormControlSelect1">Phone</label>
    <input type="phone" class="form-control" value="{{$user->mobile}}" name="mobile"  placeholder="Enter Phone Number" required="">
    
  </div>

  {{-- <div class="form-group">
    <label for="exampleFormControlSelect1"></label>
    <input type="text" class="form-control" value="{{$user->actualprice}}" name="actualprice"  placeholder="actualprice" required="">
    
  </div>

   <div class="form-group">
    <label for="exampleFormControlSelect1">Selling Price</label>
    <input type="text" class="form-control" name="SellingPrice" value="{{$user->sellingprice}}"  placeholder="Selling Price" required="">
    
  </div>

  <div class="form-group">
    <label for="exampleFormControlSelect1">ScreenPro Price</label>
    <input type="text" class="form-control" name="ScreenproPrice"  value="{{$user->screenproPrice}}" placeholder="Screenpro Price" required="">
    
  </div>
   
  <div class="form-group">
    <label>text about model(*optional)</label>
     <textarea class="textarea" name="about" rows="3" placeholder="Write something about model"
                             >{!!$row->about!!}</textarea>
                
  </div>
  <div> --}}
<button class="btn btn-success" type="submit">submit</button>


</div>

<br>
</form>

</div>
</div>
@endforeach
    <script src="https://cdn.ckeditor.com/4.11.4/standard/ckeditor.js"></script>
  {{-- <script>
            
            CKEDITOR.replace( 'about' );
            CKEDITOR.replace( 'about' );

</script> --}}

@endsection