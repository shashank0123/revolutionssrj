@extends('layouts.Dashboard')

@section('content')
 <div class="col-sm-12" style="border:5px solid black;">
 	<h3 style="text-align: center;"><u><b>Edit About us </b></u></h3>
 	
 	
 	<br>
 	<!--@foreach($show as $value)-->
 	<form action="/admin/About-edit/{{$show->id}}" method="post" enctype="multipart/form-data">
 	 		@csrf
  
  <div class="form-group">
  	 <textarea class="textarea" name="about" rows="6" placeholder="Write something about model">{{$show->about}}</textarea>
                
  </div>
  <div>
<button class="btn btn-success" type="submit">submit</button>
</div>

<br>
</div>
</div>
<!--@endforeach-->

<script src="https://cdn.ckeditor.com/4.11.4/standard/ckeditor.js"></script>
  <script>
            
            // CKEDITOR.replace( 'about' );
            CKEDITOR.replace( 'about' );

</script>
@endsection