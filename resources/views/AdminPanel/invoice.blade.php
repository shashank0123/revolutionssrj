<?php 
use App\brandDetails;
use App\brandModel;
$i=1;
 ?>
@extends('layouts.Dashboard')

@section('content')
<style>
	.invoice { border: 1px solid #000 ; width: 98%; background-color: #eee;    }
	.head,.invoice-detail,.invoice-head,.receiver-head { border-bottom: 1px solid #000;  }
	.address { text-align: right; }
	.address p { line-height: 0.7;color: #000; }
</style>
<button onclick="window.print();return false;" >Print Invoice</button>
<div class="container invoice" style="background: bisque;">

	{{-- Header --}}
	<div class="head">
		<div class="row">
			<div class="col-sm-6">
				<div class="logo" style="
    background-color: bisque;    padding: 1rem;
">
					<image src="{{ asset('logo/spazemeInvoiceLogo.png') }}" width="180" height="50">
					</div>
				</div>
				<div class="col-sm-6">
					<div class="address">
						<!--<p>Refurbind Tech Private Limited</p>-->
						 
						<p>Plot No, 20-B, Street 2, Sangam Vihar, Raj Nagar, Loni, Ghaziabad, Uttar Pradesh 201102</p>
						<p>CIN : UP29D001117</p>
					</div>
				</div>
			</div>		
		</div>

		{{-- Invoice Number --}}
		<div class="invoice-head">
			<div class="row">
				<div class="col-sm-6">
					<div class="invoice-data">
						<div class="row">
							<div class="col-sm-6 col-xs-6">Invoice Number</div>
							<div class="col-sm-6 col-xs-6">: {{$user->InvoiceNumber}}</div>
							<div class="col-sm-6 col-xs-6">Invoice Date</div>
							<div class="col-sm-6 col-xs-6">: {{$user->created_at}}</div>
							<div class="col-sm-6 col-xs-6">Tax is payable on Reserve Charge</div>
							<div class="col-sm-6 col-xs-6">: </div>
							<div class="col-md-2 col-sm-2 col-xs-6">PAN</div>
							<div class="col-md-4 col-sm-4 col-xs-6">: </div>
							<div class="col-md-1 col-sm-1 col-xs-6">CIN</div>
							<div class="col-md-5 col-sm-5 col-xs-6">:  UP29D001117</div>				
						</div>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="service-date">
						<div class="row">
							<div class="col-sm-6 col-xs-6">Service</div>
							<div class="col-sm-6 col-xs-6">: {{$user->order_id}}</div>
							<div class="col-sm-6 col-xs-6">Order ID</div>
							<div class="col-sm-6 col-xs-6">: {{$user->order_id}}</div>
							<div class="col-sm-6 col-xs-6">Date & Time of Service</div>
							<div class="col-sm-6 col-xs-6">: {{$user->service_time}}</div>
							<div class="col-sm-6 col-xs-6">Place of Supply</div>
							<div class="col-sm-6 col-xs-6">: {{$user->address}}</div>
						</div>				
					</div>
				</div>
			</div>		
		</div>

		{{-- Invoice detail --}}
		<div class="invoice-detail">
			<div class="row">
				<div class="col-sm-6">
					<div class="receiver-head">Details of Receiver (Billed To)</div>
					<div class="receiver-desc">
						<div class="row">
							<div class="col-sm-4 col-xs-6">Name </div>
							<div class="col-sm-8 col-xs-6">: {{$user->username}}</div>
							<div class="col-sm-4 col-xs-6">Mobile</div>
							<div class="col-sm-8 col-xs-6">: {{$user->phone}}</div>
							<div class="col-sm-4 col-xs-6">Address</div>
							<div class="col-sm-8 col-xs-6">: {{$user->address}}</div>
						</div>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="consignee-head">Details of Consignee (Shipped To)</div>
					<div class="consignee-desc">
						<div class="col-sm-4 col-xs-6">Name </div>
							<div class="col-sm-6 col-xs-6">: </div>
							<div class="col-sm-6 col-xs-6">Address</div>
							<div class="col-sm-6 col-xs-6">: </div>
							<div class="col-sm-6 col-xs-6">GSTIN </div>
							<div class="col-sm-6 col-xs-6">: </div>
					</div>
				</div>
			</div>		
		</div>

		{{-- Invoice Table --}}
		<div class="invoice-table">
			<table style="width: 100%;">
				<thead>
					<tr>
						<th>S. No.</th>
						<th>Brand</th>
						<th>Model</th>
						<th>Color</th>
						<th>Market Price</th>
						<th>Spazeme Price</th>
						<!--<th>Discount</th>-->
						<th>Total</th>						
					</tr>
				</thead>

				<tbody>
					
					<?php $brand_id = brandDetails::where('brand_name',$user->Brand_Name)->first();
						$model = brandModel::where('brand_id',$brand_id->id)->first();
					 ?>
					<tr>
						<td>{{$i++}}</td>
						<td>{{$user->Brand_Name}}</td>
						<td>{{$user->Brand_Model}}</td>
						<td>{{$user->Brand_color}}</td>
						<td>{{$model->actualprice}}</td>
						<td>{{$user->price}}</td>
						<!--<td>{{$model->actualprice - str_replace('RS', ' ', $user->price) }}</td>-->
						<td>{{$user->price}}</td>
					</tr>
				</tbody>
			</table>	
		</div>

	</div>

	@endsection