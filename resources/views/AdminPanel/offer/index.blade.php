@extends('layouts.Dashboard')

@section('content')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>

	<div class="container">
    <div class="row">
      <div class="col-md-5">
        <a href="offers/create" class="btn btn-info">Create</a>
      </div>
      <div class="col-md-7">
        <h4>Offer Details</h4>
      </div>
    </div>
    <div class="table-responsive text-capitalize">
      <!--Table-->
      <table class="table table-striped table-bordered" style="width: 88%; overflow: auto">

        <thead style="background: #0066ff; color: #fff">
          <tr>
            <th scope="col">S.no.</th>
            <th scope="col">Name</th>
            <th scope="col">Code</th>
            <th scope="col">Type</th>
            <th scope="col">Price</th>
            <th scope="col">Offer On</th>
						<th scope="col">Status</th>
            <th scope="col">Edit</th>
            <th scope="col">Delete</th>
           </tr>
        </thead>
        @if($offers)
        @forelse($offers as $row)
        <tbody>
          <tr scope="col" style="background: #e6f2ff;">
            <td>{{ $loop->iteration }}</td>
            <td>{{$row->offer_name}}</td>
            <td> {{$row->offer_code}} </td>
            <td>{!!$row->offer_type!!}</td>
						<td>{{$row->offer_amount}}</td>
						<td> {{$row->offer_on}} </td>
            <td> {{$row->offer_status}} </td>
            <td><a href="/admin/offers/{{$row->id}}/edit" class="btn btn-info">edit</a></td></td>
            <!-- <td><a href="services/{{$row->id}}/edit" class="btn btn-danger">Delete</a></td> -->
						<td>
							<button type="button" data-myid="{{$row->id}}" class="btn btn-danger" data-toggle="modal"
				          data-target="#deleteModal" data-whatever="@mdo">Delete</button>
						</td>
          </tr>
        </tbody>
       @empty
       <tbody>
         <tr scope="col" style="background: #e6f2ff;">
           <td colspan="8" class="text-center">
              <p style="color: red;">No Data Found</p></td>
         </tr>
       </tbody>
        @endforelse
        @endif
      </table>
	   </div>
	</div>
	<!-- START delete model -->
	<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="deleteModalLabel">Delete</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
	        <form action="offer-delete" method="POST">
						@method('DELETE')
	          @csrf
	          <div class="form-group">
	            <input type="text" name="id" class="form-control hidden" id="id">
	          </div>
						<div class="form-group">
							<h3>Are you sure want to delete this</h3>
						</div>
	          <div class="modal-footer">
			        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
			        <button type="submit"  class="btn btn-primary">Delete</button>
			      </div>
	        </form>
	      </div>
	    </div>
	  </div>
	</div>
	<!-- END delete model -->

	<script type="text/javascript">
	// delete confirm model method on open model
	$('#deleteModal').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget) // Button that triggered the modal
		var id = button.data('myid')
		var modal = $(this)
		modal.find('.modal-body #id').val(id)
	})
	</script>

@endsection
