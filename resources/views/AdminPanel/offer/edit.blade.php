@extends('layouts.Dashboard')

@section('content')

<style media="screen">
  .d-none{
    display: none;
  }
</style>
<script src="http://demo.itsolutionstuff.com/plugin/jquery.js"></script>

<div class="container-fluid" style="border:5px solid black;"style="background: #fff">
  <div class="row" >
   	<div class="col-sm-12" >
   	<h3 style="text-align: center;"><u><b>Edit Offer</b></u></h3>

   </div>
  </div>
  <form method="POST" action="{{route('offers.update', $offer->id)}}">
      @csrf
      @method('PUT')
      <div class="row mb-2">
        <div class="form-group col-md-4">
            <label for="offer_name" class="col-form-label text-md-right">{{ __('Offer Name') }} <span class="text-danger"> *</span></label>
            <input id="offer_name" type="text" class="form-control @error('offer_name') is-invalid @enderror" name="offer_name" value="{{ $offer->offer_name }}" required autocomplete="offer_name" autofocus>
            @error('offer_name')
                <span class="invalid-feedback text-danger" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>

        <div class="form-group col-md-4">
            <label for="offer_code" class="col-form-label text-md-right">{{ __('Offer Code') }} <span class="text-danger"> *</span></label>
            <input id="offer_code" type="text" class="form-control @error('offer_code') is-invalid @enderror" name="offer_code" value="{{ $offer->offer_code}}" required autocomplete="offer_code" autofocus>
            @error('offer_code')
                <span class="invalid-feedback text-danger" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>

        <div class="form-group col-md-4">
            <label for="offer_amount" class="col-form-label text-md-right">{{ __('Offer Price') }} <span class="text-danger"> *</span></label>
            <input id="offer_amount" type="number" class="form-control @error('offer_amount') is-invalid @enderror" name="offer_amount" value="{{$offer->offer_amount}}" required autocomplete="offer_amount" autofocus>
            @error('offer_amount')
                <span class="invalid-feedback text-danger" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>

      </div>

      <div class="row mb-2">
        <div class="form-group col-md-4">
            <label for="offer_type" class=" col-form-label text-md-right">{{ __('Offer Type') }} <span class="text-danger"> *</span></label>
            <select id="offer_type" type="text" class="form-control @error('offer_type') is-invalid @enderror" name="offer_type" value="{{$offer->offer_type}}" required autocomplete="offer_type" >
              <option value="" >Select Offer Type</option>
              <option value="all" selected="{{$offer->offer_type}}">All</option>
              <option value="brand" selected="{{$offer->offer_type}}">Brand</option>
              <option value="model" selected="{{$offer->offer_type}}">Model</option>
            </select>
            @error('offer_type')
                <span class="invalid-feedback text-danger " role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>

        <div id="brand_div" class="form-group col-md-4 d-none">
            <label for="brand_id" class="col-form-label text-md-right">{{ __('Brand Name') }} <span class="text-danger"> *</span></label>
            <select id="brand_id" class="form-control @error('brand_id') is-invalid @enderror" name="brand_id" value="{{$offer->brand_id}}" >
              <option value="" class="d-none">Select Model</option>
              @if($brands)
                @foreach($brands as $value)
                  <option value="{{$value->id}}" selected="{{$offer->brand_id}}">{{$value->brand_name}}</option>
                @endforeach
              @endif
            </select>
            @error('brand_id')
                <span class="invalid-feedback text-danger" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>

        <div id="model_div" class="form-group col-md-4 d-none">
            <label for="model_id" class="col-form-label text-md-right">{{ __('Model Name') }} <span class="text-danger"> *</span></label>
            <select id="model_id" class="form-control @error('model_id') is-invalid @enderror" name="model_id" value="{{$offer->model_id }}">
              <option value="" class="d-none">Select Model</option>
              @if($models)
                @foreach($models as $value)
                  <option value="{{$value->id}}" selected="{{$offer->model_id}}">{{$value->brand_model_name}}</option>
                @endforeach
              @endif
            </select>
            @error('model_id')
                <span class="invalid-feedback text-danger" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>

        <div class="form-group col-md-4">
            <label for="offer_status" class=" col-form-label text-md-right">{{ __('Status') }} <span class="text-danger"> *</span></label>
            <select id="offer_status" type="text" class="form-control @error('offer_status') is-invalid @enderror" name="offer_status" value="{{$offer->offer_status }}" required autocomplete="offer_status" >
              <option value="" >Select Status</option>
              <option value="active" @if($offer->offer_status == 'active') selected @endif>Active</option>
              <option value="inctive" @if($offer->offer_status == 'inctive') selected @endif>Inactive</option>
            </select>
            @error('offer_status')
                <span class="invalid-feedback text-danger " role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
      </div>
      <div class="form-group row mb-0">
          <div class="col-md-6 offset-md-4">
              <button type="submit" class="btn btn-primary">
                  {{ __('Submit') }} {{$offer->brand_id}}
              </button>
          </div>
      </div>
  </form>
</div>
<script type="text/javascript">
// A $( document ).ready() block.
$( document ).ready(function() {
  @if($offer->brand_id)
    $('#brand_div').removeClass('d-none');
    console.log('hi');
  @endif
  @if($offer->model_id)
    $('#model_div').removeClass('d-none');
  @endif
});
  $('#offer_type').on('change', function(e){
    e.preventDefault();
    var value = $(this).val();
    switch (value) {
      case 'all':
      // var id = $('#model_div').attr('class');
        $('#model_div').removeClass();
        $('#model_div').addClass('form-group col-md-4 d-none');
        $('#brand_div').removeClass();
        $('#brand_div').addClass('form-group col-md-4 d-none');
        break;

      case 'brand':
      // var id = $('#model_div').attr('class');
        $('#model_div').removeClass();
        $('#model_div').addClass('form-group col-md-4 d-none');
        $('#brand_div').removeClass();
        $('#brand_div').addClass('form-group col-md-4');
        break;

      case 'model':
        $('#model_div').removeClass();
        $('#model_div').addClass('form-group col-md-4');
        $('#brand_div').removeClass();
        $('#brand_div').addClass('form-group col-md-4 d-none');
        break;

      default:

    }
  });
</script>
@endsection
