@extends('layouts.Dashboard')

@section('content')
<div class="container-fluid" style="border:5px solid black;"style="background: #fff">
  <div class="row" >
   	<div class="col-sm-12" >
   	<h3 style="text-align: center;"><u><b>Create Service</b></u></h3>

   </div>
  </div>
  <form method="POST" action="{{route('model-services.store')}}" enctype="multipart/form-data">
      @csrf
      <input type="text" name="service_id" value="{{$serviceId}}" hidden>
      <div class="row mb-2">
        <div class="form-group col-md-6">
            <label for="model_id" class="col-form-label text-md-right">{{ __('Model Name') }} <span class="text-danger"> *</span></label>
            <select id="model_id" class="form-control @error('model_id') is-invalid @enderror" name="model_id" value="{{ old('model_id') }}" required >
              <option value="" class="d-none">Select Model</option>
              @if($modelData)
                @foreach($modelData as $value)
                  <option value="{{$value->id}}">{{$value->brand_model_name}}</option>
                @endforeach
              @endif
            </select>
            <!-- <input id="model_id" type="text" class="form-control @error('model_id') is-invalid @enderror" name="model_id" value="{{ old('model_id') }}" required autocomplete="model_id" autofocus> -->

            @error('model_id')
                <span class="invalid-feedback text-danger" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>

          <div class="form-group col-md-6">
              <label for="status" class=" col-form-label text-md-right">{{ __('Status') }} <span class="text-danger"> *</span></label>
              <!-- <input id="status" type="text" class="form-control @error('status') is-invalid @enderror" name="status" value="{{ old('status') }}" required autocomplete="status"> -->
              <select id="status" type="text" class="form-control @error('status') is-invalid @enderror" name="status" value="{{ old('status') }}" required autocomplete="status" >
                <option value="active" >Active</option>
                <option value="inctive">Inactive</option>
              </select>
              @error('status')
                  <span class="invalid-feedback text-danger " role="alert">
                      <strong>{{ $message }}</strong>
                  </span>
              @enderror
          </div>
      </div>

      <div class="row mb-2">
        <div class="form-group col-md-6">
            <label for="actual_price" class="col-form-label text-md-right">{{ __('Actual Price') }} <span class="text-danger"> *</span></label>
            <input id="actual_price" type="number" class="form-control @error('actual_price') is-invalid @enderror" name="actual_price" value="{{ old('actual_price') }}" required autocomplete="actual_price" autofocus>
            @error('actual_price')
                <span class="invalid-feedback text-danger" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>

          <div class="form-group col-md-6">
              <label for="our_price" class=" col-form-label text-md-right">{{ __('Spazeme Price') }} <span class="text-danger"> *</span></label>
              <input id="our_price" type="number" class="form-control @error('our_price') is-invalid @enderror" name="our_price"
              value="{{ old('our_price') }}" required autocomplete="our_price">
              @error('our_price')
                  <span class="invalid-feedback text-danger" role="alert">
                      <strong>{{ $message }}</strong>
                  </span>
              @enderror
          </div>
      </div>
      <div class="form-group row mb-0">
          <div class="col-md-6 offset-md-4">
              <button type="submit" class="btn btn-primary">
                  {{ __('Submit') }}
              </button>
          </div>
      </div>
  </form>
</div>
@endsection
