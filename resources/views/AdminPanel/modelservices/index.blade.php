@extends('layouts.Dashboard')

@section('content')
	<div class="container">
    <div class="row">
      <div class="col-md-5">
        <a href="model-services/create?serviceId={{$serviceId}}" class="btn btn-info">Create</a>
      </div>
      <div class="col-md-7">
        <h4>Models of {{$serviceName}} Service</h4>
      </div>
    </div>
    <div class="row mb-rem-1 search-content-row" >
      <form>
				<input type="text" name="serviceId" hidden value="{{$serviceId}}">
        <div class="col-md-3">
          <input type="text" class="form-control mb-2" value="{{request('service_name')}}" autocomplete="off" placeholder="Enter Model Name" name="service_name" style="font-size: 14px;">
        </div>
				<div class="col-md-2">
          <select name="service_status" class="form-control" id="service_status" value="{{request('service_status')}}">
            <option value="all" @if(request('service_status') == 'all') selected @endif>All</option>
            <option value="active" @if(request('service_status') == 'active') selected @endif >Active</option>
            <option value="inactive" @if(request('service_status') == 'inactive') selected @endif >Inactive</option>
          </select>
        </div>
        <div class="col-md-2">
          <select name="order_by_column" class="form-control" id="order_by_column" value="{{request('order_by_column')}}">
            <option value="all" @if(request('order_by_column') == 'all') selected @endif >Default</option>
						<option value="created_at" @if(request('order_by_column') == 'created_at')@endif >Created At</option>
            <option value="service_name" @if(request('order_by_column') == 'service_name') selected @endif>Service Name</option>
          </select>
        </div>
				<div class="col-md-3">
          <select name="order_by_value" class="form-control" id="order_by_value" value="{{request('order_by_value')}}">
            <option value="all" @if(request('order_by_value') == 'all') selected @endif >Default</option>
						<option value="desc" @if(request('order_by_value') == 'desc') selected @endif >Descending Order</option>
            <option value="asc" @if(request('order_by_value') == 'asc') selected @endif >Ascending Order</option>
          </select>
        </div>
        <div class="col-md-2">
          <button class="btn btn-info mb-2">Search</button>
        </div>
      </form>
    </div>

    <div class="table-responsive text-capitalize">
      <!--Table-->
      <table class="table table-striped table-bordered" style="width: 88%; overflow: auto">

        <thead style="background: #0066ff; color: #fff">
          <tr>
            <th scope="col">S.no.</th>
            <th scope="col">Model Name</th>
            <th scope="col">Actual Price</th>
            <th scope="col">Spazeme Price</th>
						<th scope="col">Status</th>
            <th scope="col">Edit</th>
            <!-- <th scope="col">Delete</th> -->
           </tr>
        </thead>
        @if($modelServices)
        @forelse($modelServices as $row)
        <tbody>
          <tr scope="col" style="background: #e6f2ff;">
            <td>{{ $loop->iteration }}</td>
            <td>{{$row->brand_model_name}}</td>
            <td> {{$row->actual_price}} </td>
            <td>{!!$row->our_price!!}</td>
						<td> {{$row->status}} </td>
            <td><a href="/admin/model-services/{{$row->model_services_id}}/edit?serviceId={{$serviceId}}" class="btn btn-info">edit</a></td></td>
            <!-- <td><a href="services/{{$row->id}}/edit" class="btn btn-danger">Delete</a></td> -->
          </tr>
        </tbody>
       @empty
       <tbody>
         <tr scope="col" style="background: #e6f2ff;">
           <td colspan="6" class="text-center">
              <p style="color: red;">No Data Found</p></td>
         </tr>
       </tbody>
        @endforelse
        @endif
      </table>
	   </div>
	</div>
{{--  --}}
{!! $modelServices->appends(request()->except('page'))->links() !!}

@endsection
