@extends('layouts.Dashboard')

@section('content')
<div class="container-fluid" style="border:5px solid black;"style="background: #fff">
  <div class="row" >
   	<div class="col-sm-12" >
   	<h3 style="text-align: center;"><u><b>Edit Service Model</b></u></h3>

   </div>
  </div>
  <form method="POST" action="{{route('model-services.update', $modelServices->id)}}" enctype="multipart/form-data">
      @csrf
      @method('PUT')
      <input type="text" name="service_id" value="{{$serviceId}}" hidden>
      <div class="row mb-2">
        <div class="form-group col-md-6">
            <label for="model_id" class="col-form-label text-md-right">{{ __('Model Name') }} <span class="text-danger"> *</span></label>
            <!-- <input id="model_id" type="text" class="form-control @error('model_id') is-invalid @enderror" name="model_id" value="{{  $modelServices->model_id }}" required autocomplete="model_id" autofocus> -->
            <select id="model_id" class="form-control @error('model_id') is-invalid @enderror" name="model_id" value="{{ $modelServices->model_id }}" required>
              <option value="" class="d-none">Select Model</option>
              @if($modelData)
                @foreach($modelData as $value)
                  <option value="{{$value->id}}" @if($value->id ==$modelServices->model_id) selected @endif >{{$value->brand_model_name}}</option>
                @endforeach
              @endif
            </select>

            @error('model_id')
                <span class="invalid-feedback text-danger" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>

          <div class="form-group col-md-6">
              <label for="status" class=" col-form-label text-md-right">{{ __('Status') }} <span class="text-danger"> *</span></label>
              <!-- <input id="status" type="text" class="form-control @error('status') is-invalid @enderror" name="status" value="{{ old('status') }}" required autocomplete="status"> -->
              <select id="status" type="text" class="form-control @error('status') is-invalid @enderror" name="status" value="{{$modelServices->status }}" required autocomplete="status" >
                <option value="active" @if($modelServices->status == 'active') selected @endif >Active</option>
                <option value="inctive" @if($modelServices->status == 'inctive') selected @endif>Inactive</option>
              </select>
              @error('status')
                  <span class="invalid-feedback text-danger " role="alert">
                      <strong>{{ $message }}</strong>
                  </span>
              @enderror
          </div>
      </div>

      <div class="row mb-2">
        <div class="form-group col-md-6">
            <label for="actual_price" class="col-form-label text-md-right">{{ __('Actual Price') }} <span class="text-danger"> *</span></label>
            <input id="actual_price" type="number" class="form-control @error('actual_price') is-invalid @enderror" name="actual_price" value="{{ $modelServices->actual_price }}" required autocomplete="actual_price" autofocus>
            @error('actual_price')
                <span class="invalid-feedback text-danger" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>

          <div class="form-group col-md-6">
              <label for="our_price" class=" col-form-label text-md-right">{{ __('Spazeme Price') }} <span class="text-danger"> *</span></label>
              <input id="our_price" type="number" class="form-control @error('our_price') is-invalid @enderror" name="our_price"
              value="{{$modelServices->our_price }}" required autocomplete="our_price">
              @error('our_price')
                  <span class="invalid-feedback text-danger" role="alert">
                      <strong>{{ $message }}</strong>
                  </span>
              @enderror
          </div>
      </div>
      <div class="form-group row mb-0">
          <div class="col-md-6 offset-md-4">
              <button type="submit" class="btn btn-primary">
                  {{ __('Submit') }}
              </button>
          </div>
      </div>
  </form>
</div>
@endsection
