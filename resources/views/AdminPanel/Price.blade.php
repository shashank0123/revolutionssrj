<?php
use App\brandDetails;
?>

@extends('layouts.Dashboard')

@section('content')
<div class="card-body">
	@if (session('status'))
	<div class="alert alert-success" role="alert">
		{{ session('status') }}
	</div>
	@endif
	<div class="alert alert-success">

		<p>Price</p>

	</div>

	<div class="table-responsive ">
        <!--Table-->
        <table class="table table-striped table-bordered" style="width: 88%; overflow: auto">

          <thead style="background: #0066ff; color: #fff">
  
    <tr>

      <th scope="col">S.no.</th>
      <th scope="col">Brand Name</th>
      <th scope="col">Brand Model</th>
      <th scope="col">Actual Price</th>
      <th scope="col">Selling Price</th>
      <th scope="col">SpazeMe Price</th>
      {{-- <td scope="col">Edit</td> --}}
      {{-- <td scope="col">Delete</td> --}}

     </tr>
  </thead>
  <?php $i=1; ?>
  @forelse($modals as $modal)

  <tbody>
  	
  	<tr scope="col" style="background: #e6f2ff;"> 

  		<td>{{ $i++ }}</td>
     <?php $brand=brandDetails::where('id',$modal->brand_id)->first(); ?>
      <td>{{$brand->brand_name}} </td>

      
      <td>{{$modal->brand_model_name}}</td>
      <td>Rs. {{$modal->actualprice}}</td>
      <td>Rs. {{$modal->sellingprice}}</td>
      <td>Rs. {{$modal->screenproPrice}}</td>
     {{-- <td>
      <button type="button" class="btn btn-primary" href="/admin/edit-modal-detail/{{$modal->id}}">Edit</button>

      <button type="button" data-myid="{{$modal->id}}" data-myname="{!!$modal->name!!}" data-mymodel="{{$modal->brand_model_name}}" data-myemail="{{$modal->email}}" data-mymobile="{{$modal->mobile}}"  class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo">Edit</button></td> --}}
      {{-- <td><button type="button" href="/admin/modal-lists/{{$modal->id}}" class="btn btn-danger">Delete</a></td> --}}

	</tr>
  	
  </tbody>
 @empty
    <p style="color: red;">No  Details</p>
  @endforelse 
</table>
	</div>

</div>
@endsection