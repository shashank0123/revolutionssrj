@extends('layouts.Dashboard')

@section('content')
<div class="container-fluid" style="border:5px solid black;"style="background: #fff">
  <div class="row" >
   	<div class="col-sm-6" >
   	<h3 style="text-align: center;"><u><b>Create Testimonial</b></u></h3>

   </div>
  </div>
  <form method="POST" action="{{ route('testimonial.store') }}" enctype="multipart/form-data">
      @csrf
      <input type="text" name="city" hidden value="Bhind M.P.">
      <div class="row mb-2">
        <div class="form-group col-md-6">
            <label for="client_name" class="col-form-label text-md-right">{{ __('Client Name') }}</label>

            <input id="client_name" type="text" class="form-control @error('client_name') is-invalid @enderror" name="client_name" value="{{ old('client_name') }}" required autocomplete="client_name" autofocus>

            @error('client_name')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>

        <div class="form-group col-md-6">
            <label for="image_url" class="col-form-label text-md-right">{{ __('Image') }}</label>

            <input id="image_url" type="file" class="form-control @error('image_url') is-invalid @enderror" name="image_url" value="{{ old('image_url') }}" required autocomplete="image_url" autofocus>

            @error('image_url')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
      </div>

      <div class="row mb-2">
        <div class="form-group col-md-6">
            <label for="rating" class="col-form-label text-md-right">{{ __('Rating') }}</label>
            <input id="rating" type="number" class="form-control @error('rating') is-invalid @enderror" name="rating" value="{{ old('rating') }}" required autocomplete="rating" autofocus>
            @error('rating')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
        <div class="form-group col-md-6">
            <label for="status" class=" col-form-label text-md-right">{{ __('Status') }}</label>
            <select id="status" type="text" class="form-control @error('status') is-invalid @enderror" name="status" value="{{ old('status') }}" required autocomplete="status" >
              <option value="active" >Active</option>
              <option value="inctive">Inactive</option>
            </select>
            @error('status')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
      </div>

      <div class="row mb-2">
        <div class="form-group col-md-12">
            <label for="review" class="col-form-label text-md-right">{{ __('Review') }}</label>
            <!-- <input id="review" type="text" class="form-control @error('review') is-invalid @enderror" name="review" value="{{ old('review') }}" required autocomplete="review" autofocus> -->
            <textarea rows="4" cols="80" id="review" type="text" class="form-control @error('review') is-invalid @enderror" name="review" value="{{ old('review') }}" required autocomplete="review" autofocus>
              {{ old('review') }}
            </textarea>

            @error('review')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
      </div>

      <div class="form-group row mb-0">
          <div class="col-md-6 offset-md-4">
              <button type="submit" class="btn btn-primary">
                  {{ __('Submit') }}
              </button>
          </div>
      </div>
  </form>
</div>
@endsection
