@extends('layouts.Dashboard')

@section('content')


	<div class="container">
     <div style="text-align: center;" ><h3><u><b>Contact message </b></u></h3></div>
     <div class="form-group">
                
             </div>
    <div class="table-responsive ">
        <!--Table-->
        <table class="table table-striped table-bordered">

          <thead style="background: #0066ff; color: #fff">
  
    <tr class="text-capitalize">

      <th scope="col">S.no.</th>
      <th scope="col">Name</th>
      <th scope="col">Email</th>
      <th scope="col">phone</th>
      <th scope="col">query</th>
      <th scope="col">message</th>
      <th scope="col">Status</th>
      <th scope="col">Time</th>
      <th scope="col">Action</th>
     </tr>
  </thead>
  <tbody>
  	@forelse($data as $row)
  	<tr scope="col" style="background: #e6f2ff;"> 

  		<td>{{ $loop->iteration }}</td>
  		<td>{{$row->name}}</td>
  		<td>{{$row->email }}</td>
  		<td>{{$row->phone }}</td>
  		<td>${{$row->query}}</td>
  		<td>{!!$row->message!!}</td>
  		<td>{{$row->Action}}</td>
  		<td>{{$row->created_at}}</td>
  		<td><button type="button" data-myid="{{$row->id}}" data-myemail="{{$row->email}}"  class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo">{{$row->Action}}</button></td>
	</tr>
  	
  </tbody>
 @empty
  	<p style="color: red;">No Message</p>
  @endforelse
</table>
	</div>
	</div>



<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add UserRole</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="" method="post" enctype="multipart/form-data">
          @csrf
          <div class="form-group">
            
            <input type="hidden" name="id" class="form-control" id="id">
          </div>
           <div class="form-group">
           
            <input type="text" name="email" class="form-control" id="email">
          </div>
          <div class="form-group">
            <label for="message-text" class="col-form-label">Pending/complete</label>
            <select class="form-control" name="role">
              <option name="role" value="1">pending</option>
              <option name="role" value="0">complete</option>

            </select>
            
          </div>
           <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit"  class="btn btn-primary">Apply</button>
      </div>
        </form>
      </div>
     
    </div>
  </div>
</div>   
 
<script type="text/javascript">
$('#exampleModal').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget) // Button that triggered the modal
  var recipient = button.data('myid')
  var email = button.data('myemail') // Extract info from data-* attributes
  var modal = $(this)
 
  modal.find('.modal-body #id').val(recipient)
   modal.find('.modal-body #email').val(email)
})


    </script>
    
@endsection