@extends('layouts.Dashboard')

@section('content')

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
	<div class="container">
     <div style="text-align: center;" ><h3><u><b>Edit Brand Name</b></u></h3></div>
     <div class="row mb-rem-1 search-content-row" >
       <form>
         <div class="col-md-4">
           <input type="text" class="form-control mb-2" value="{{request('brand_name')}}" autocomplete="off" placeholder="Enter Brand Name" name="brand_name" style="font-size: 14px;">
         </div>
 				<div class="col-md-2">
           <select name="service_status" class="form-control" id="service_status" value="{{request('service_status')}}">
             <option value="all" @if(request('service_status') == 'all') selected @endif>All</option>
             <option value="active" @if(request('service_status') == 'active') selected @endif >Active</option>
             <option value="inactive" @if(request('service_status') == 'inactive') selected @endif >Inactive</option>
           </select>
         </div>
         <div class="col-md-2">
           <select name="order_by_column" class="form-control" id="order_by_column" value="{{request('order_by_column')}}">
             <option value="all" @if(request('order_by_column') == 'all') selected @endif >Default</option>
 						<option value="created_at" @if(request('order_by_column') == 'created_at')@endif >Created At</option>
             <option value="service_name" @if(request('order_by_column') == 'service_name') selected @endif>Service Name</option>
           </select>
         </div>
 				<div class="col-md-2">
           <select name="order_by_value" class="form-control" id="order_by_value" value="{{request('order_by_value')}}">
             <option value="all" @if(request('order_by_value') == 'all') selected @endif >Default</option>
 						<option value="desc" @if(request('order_by_value') == 'desc') selected @endif >Descending Order</option>
             <option value="asc" @if(request('order_by_value') == 'asc') selected @endif >Ascending Order</option>
           </select>
         </div>
         <div class="col-md-2">
           <button class="btn btn-info mb-2">Search</button>
         </div>
       </form>
     </div>
    <div class="table-responsive ">
        <!--Table-->
        <table class="table table-striped table-bordered">

          <thead style="background: #0066ff; color: #fff">
  
    <tr>

      <th scope="col">S.no.</th>
      <th scope="col">Name</th>
      <th scope="col">Image</th>
      <td scope="col">Edit</td>
      <td scope="col">Delete</td>

     </tr>
  </thead>
  @forelse($show as $row)
  <tbody>
  	
  	<tr scope="col" style="background: #e6f2ff;"> 

  		<td>{{ $loop->iteration }}</td>
      <td>{{ $row->brand_name }}</td>
      <td><img src="/admin/images/{{$row->brand_image}}" width="60px" height="60px"></td>
  		<td>
  		    <button type="button" data-myid="{{$row->id}}" data-myname="{{$row->brand_name}}" data-title="{{$row->brand_title}}" data-disc="{{$row->brand_disc}}" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo">Edit</button>
  		    </td>
      <td><a href="/admin/brand_details/{{$row->id}}" class="btn btn-danger">Delete</a></td>

	</tr>
  	
  </tbody>
 @empty
    <p style="color: red;">No  Details</p>
  @endforelse 
</table>
	</div>
	</div>



<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Update</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="/admin/brand_name-edit" method="post" enctype="multipart/form-data">
          @csrf
          <div class="form-group">
            
            <input type="text" name="id" class="form-control hidden" id="id">
          </div>
          <div class="form-group">
            
            <input type="text" name="brand_name" class="form-control" id="name">
          </div>
           <div class="form-group">
           
            <input type="file" name="images" class="form-control" id="email">
          </div>
            <div class="form-group">
                <label for="brand_title">Brand Title</label>
                <input type="text" class="form-control" name="brand_title" id="brand_title" placeholder="Type Brand Title" required>
              </div>
              <div class="form-group">
                <label>Brand Discription (*optional)</label>
                <textarea class="form-control textarea" name="brand_disc" id="brand_disc"  rows="3" placeholder="Write something about brand" ></textarea>
              </div>
          
          <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit"  class="btn btn-primary">Update</button>
      </div>
        </form>
      </div>
     
    </div>
  </div>
</div>   
{!! $show->links() !!}
<script type="text/javascript">
$('#exampleModal').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget) // Button that triggered the modal
  var recipient = button.data('myid')
  var name = button.data('myname')
  var brand_title = button.data('title')
  var brand_disc = button.data('disc')  // Extract info from data-* attributes
  var modal = $(this)
 
  modal.find('.modal-body #id').val(recipient)
  modal.find('.modal-body #name').val(name)
  modal.find('.modal-body #brand_title').val(brand_title)
  modal.find('.modal-body #brand_disc').html(brand_disc)
    // document.getElementById("brand_disc").value = brand_disc
    // $('#brand_disc').append(brand_disc);
    // $("textarea#brand_disc").html(brand_disc);
    // document.getElementById("brand_disc").value = "Fifth Avenue, New York City";

})


    </script>
    
@endsection