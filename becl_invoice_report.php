<?php
include("../library/raso_function.php");


if(isset($_GET['student_id']) && $_GET['student_id']!="" ){
	$id = isset($_GET['student_id'])?addslashes($_GET['student_id']):'';

	$sel_fee=exeQuery("select * from student INNER JOIN fees_transaction
ON student.Id = fees_transaction.stdid where student.courses='".$_GET['course']."' AND student.Id='".$id."' order by fees_transaction.submitdate desc");

	$numrows = num_res($sel_fee);

	$sel_student=exeQuery("select * from ".TABLE_STUDENT." where id='".$id."' ");
	$res_student=fetchAssoc($sel_student);

}else{
	header("location: invoice.php");
}




?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<title><?php echo SITENAME;?></title>

	<!-- BOOTSTRAP STYLES-->
	<link href="css/bootstrap.css" rel="stylesheet" />
	<!-- FONTAWESOME STYLES-->
	<link href="css/font-awesome.css" rel="stylesheet" />
	<!--CUSTOM BASIC STYLES-->
	<link href="css/basic.css" rel="stylesheet" />
	<!--CUSTOM MAIN STYLES-->
	<link href="css/custom.css" rel="stylesheet" />



	<!-- GOOGLE FONTS-->
	<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />

	<script src="js/jquery-1.10.2.js"></script>



</head>
<?php
include("php/header.php");
?>
<div id="page-wrapper">
	<div id="page-inner">
		<div class="row">
			<div class="col-md-12">
				<h1 class="page-head-line">Invoice
					Reports
				</h1>


			</div>
		</div>



			<link href="css/datatable/datatable.css" rel="stylesheet" />
			<!-- student info -->

			<div class="panel panel-default">
				<div class="panel-heading">
					<b>Student Info</b>
				</div>
				<div class="panel-body">
				<?php if($numrows > 0){?>
					<div class="table-sorting table-responsive">

						<table class="table table-striped table-bordered table-hover" id="tSortable22">
							<thead>
								<tr>
									<th>#</th>
									<th>Name</th>
									<th>Branch / Course</th>
									<th>Roll No</th>
								</tr>
							</thead>
							<tbody>
								<?php
								$i=1;
								while($r = fetchAssoc($sel_fee))
								{
									$sql_branch=exeQuery("select * from branch where status!='0' AND id='".$res_student['branch']."'");
									$res_branch=fetchAssoc($sql_branch);

									$sql_course=exeQuery("select * from courses where status!='0' AND id='".$res_student['courses']."'");
									$res_course=fetchAssoc($sql_course);
									?>
									<tr>
										<td><?php echo $i;?></td>
										<!--<td><?php echo ($res_student['image']!="" and file_exists("../upload/".$res_student['image']))?"<img  src='../upload/".$res_student['image']."' width='100' alt='".$res_student['fullname']."'>":"<img  src='../upload/1564130580lg.jpg' width='100' alt='".$res_student['fullname']."' title='".$res_student['fullname']."'";?></td>-->
										<td><?php echo $res_student['fullname'];?></td>
										<td><?php echo $res_branch['branch_name'];?> / <?php echo $res_course['course_name'];?></td>
										<td><?php echo $res_student['reno'];?></td>
										</tr>

										<?php
										$i++;
									}
									?>
								</tbody>
							</table>
				</div><?php } else{
					echo 'Records not found';
				}?>
					</div>
				</div>

			<!-- fee info -->
			<div class="panel panel-default">
				<div class="panel-heading">
					<b>Fee Info </b>
				</div>
				<div class="panel-body">
				<?php if($numrows > 0){?>
					<div class="table-sorting table-responsive">

						<table class="table table-striped table-bordered table-hover" id="tSortable22">
							<thead>
								<tr>
									<th>#</th>
									<th>Paid</th>
									<th>Balance</th>
									<th>Deposite Date</th>
								</tr>
							</thead>
							<tbody>
								<?php
									// $sql = "select * from branch where delete_status='0'";

								$i=1;
								while($r = fetchAssoc($sel_fee))
								{



									$sql_branch=exeQuery("select * from branch where status!='0' AND id='".$res_student['branch']."'");
									$res_branch=fetchAssoc($sql_branch);

									$sql_course=exeQuery("select * from courses where status!='0' AND id='".$res_student['courses']."'");
									$res_course=fetchAssoc($sql_course);
									?>
									<tr>
										<td><?php echo $i;?></td>
										<!--<td><?php echo ($res_student['image']!="" and file_exists("../upload/".$res_student['image']))?"<img  src='../upload/".$res_student['image']."' width='100' alt='".$res_student['fullname']."'>":"<img  src='../upload/1564130580lg.jpg' width='100' alt='".$res_student['fullname']."' title='".$res_student['fullname']."'";?></td>-->
										<td><?php echo $res_student['fullname'];?></td>
										<td><?php echo $res_branch['branch_name'];?> / <?php echo $res_course['course_name'];?></td>
										<td><?php echo $res_student['reno'];?></td>
										<td><?php echo "Rs.";echo $r['fees']-$r['balance'];?></td></td>
										<td><?php echo "Rs.".$r['balance'];?></td>

										<td><?php echo date('d M, Y', strtotime($r['submitdate']));?></td>

										</tr>

										<?php
										$i++;
									}
									?>



								</tbody>
							</table>
				</div><?php } else{
					echo 'Records not found';
				}?>
					</div>
				</div>

				<script src="js/dataTable/jquery.dataTables.min.js"></script>
				<script>
					$(document).ready(function () {
						$('#tSortable22').dataTable({
							"bPaginate": true,
							"bLengthChange": false,
							"bFilter": true,
							"bInfo": false,
							"bAutoWidth": true });

					});


				</script>




		</div>
		<!-- /. PAGE INNER  -->
	</div>
	<!-- /. PAGE WRAPPER  -->
</div>
<!-- /. WRAPPER  -->

<div id="footer-sec">
	Thebsel | Brought To You By : <a href="http://www.backstagesupporters.com" target="_blank">Backstagesupporters Pvt Ltd</a>
</div>


<!-- BOOTSTRAP SCRIPTS -->
<script src="js/bootstrap.js"></script>
<!-- METISMENU SCRIPTS -->
<script src="js/jquery.metisMenu.js"></script>
<!-- CUSTOM SCRIPTS -->
<script src="js/custom1.js"></script>


</body>
</html>
