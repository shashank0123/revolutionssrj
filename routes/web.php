<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// ini_set('memory_limit','512MB');
Route::get('/cron', ['as' => 'admin.cron', 'uses' => 'HomeController@runCron']);

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

// Route::get('/dashboard', 'HomeController@index')->name('dashboard');
// Route::post('adminLogin', 'AdminController@login')->name('adminLogin');

  Route::group(['prefix' => 'admin', 'middleware' =>['auth:web'] ],function () {

Route::get('/', function () {
    return view('AdminPanel.Dashboard');
});

Route::resource('services', 'ServiceController');
Route::any('services-delete', 'ServiceController@destroy');
Route::resource('model-services', 'ModelServicesController');
Route::resource('offers', 'OfferController');
Route::any('offer-delete', 'OfferController@destroy');
Route::resource('testimonial', 'TestimonialController');

Route::get('/ajax_upload', 'UserController@index');

Route::post('/ajax_upload/action', 'UserController@action')->name('ajaxupload.action');


Route::get('myform/ajax/{id}',array('as'=>'myform.ajax','uses'=>'AdminController@myformAjax'));


Route::post('/slider', 'AdminController@AddImage');

Route::get('/slider-image','AdminController@showSlider');
Route::post('/slider-image-edit','AdminController@EditSlider');
Route::get('/slider-image/delete/{id}',function($id){

	$delete=DB::delete('delete  from slider where id=?',[$id]);
	return Redirect::back()->with('success','successfully delete');

});

Route::post('/brandname','AdminController@AddBrand');
Route::get('/showbrandDetails','AdminEditController@showbrandDetails');
Route::post('/brand_name-edit','AdminEditController@editbrandDetails');
Route::get('/brand_details/{id}',function($id){

	$delete=DB::delete('delete  from brand_details where id=?',[$id]);
	return Redirect::back()->with('success','successfully delete');
});

Route::view('/questionanswer','AdminPanel/questionanswer');
Route::post('/questionAnswer','AdminController@questionAnswer');
Route::get('/showF&Q','AdminController@showqa');

Route::post('/brandmodel','AdminController@brandModel');
Route::get('/showModel','AdminEditController@showModel');
Route::post('/edit-model-page','AdminEditController@editbrandModel');
Route::get('/brand_edit-model/{id}','AdminEditController@EditModelPage');
Route::get('/brand_model/{id}', function($id){
$delete=DB::delete('delete  from brand_model where id=?',[$id]);
	return Redirect::back()->with('success','successfully delete');

});

Route::get('/terms-condition', 'AdminController@getTerms');
Route::post('/termsCondition','AdminController@terms');
Route::get('/terms','AdminController@showterms');
Route::post('/edit-termsCondition','AdminEditController@editTerms');

Route::view('/benifitsterms','AdminPanel/benifitAbout');
Route::post('/benifitsterms','AdminController@benifitabout');
Route::get('/benifit-view', 'AdminController@benifitshow');
Route::get('/benifit/edit/{id}', 'AdminController@shwbenifitshow');

// Route::post('/AvailableStatus','AdminController@Available');

Route::post('/AvailableStatus','AdminController@AvailableStatus');
Route::get('/show/AvailableStatus','AdminController@showavailable');
Route::post('/state-available-edit','AdminEditController@editStateDetails');

Route::post('/brandcolor','AdminController@brandcolor');
Route::get('/brandcolor-page','AdminEditController@brandcolor');
Route::post('/brandcolor-edit','AdminEditController@editbrandcolor');
Route::post('/brandcolor-delete','AdminEditController@deletebrandcolor');

Route::post('/About', 'AdminController@About12');

Route::view('/admin','AdminPanel/Dashboard');

Route::get('/about','AdminController@about');
Route::get('/about-edit','AdminController@showAbout');
Route::post('/About-edit/{id}','AdminController@editAbout');

Route::get('/admin-home','AdminController@home');
Route::get('/contact','AdminController@contact');

Route::get('users-list', 'AdminController@getAllUsers');
Route::post('user-delete', 'AdminEditController@deleteUser');
Route::delete('userDeleteAll', 'AdminController@deleteAll');

Route::get('orders', 'AdminController@getAllOrders');
Route::post('order-delete','AdminEditController@deleteorder');

Route::get('invoice/{id}', 'AdminController@getInvoice');

Route::get('edit-user-detail/{id}', 'AdminController@editUser');

Route::post('change-status', 'AdminController@changeOrderStatus');

Route::get('price', 'AdminController@getPrice');

Route::post('users-list/{id}',function($id){

	$delete=DB::delete('delete  from users where id=?',[$id]);
	return Redirect::back()->with('success','successfully delete');
});
});
